/*
 * Generated by the Jasper component of Apache Tomcat
 * Version: Apache Tomcat/7.0.47
 * Generated at: 2015-03-23 22:57:19 UTC
 * Note: The last modified time of this file was set to
 *       the last modified time of the source file after
 *       generation to assist with modification tracking.
 */
package org.apache.jsp.app;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.jsp.*;

public final class index_jsp extends org.apache.jasper.runtime.HttpJspBase
    implements org.apache.jasper.runtime.JspSourceDependent {

  private static final javax.servlet.jsp.JspFactory _jspxFactory =
          javax.servlet.jsp.JspFactory.getDefaultFactory();

  private static java.util.Map<java.lang.String,java.lang.Long> _jspx_dependants;

  private javax.el.ExpressionFactory _el_expressionfactory;
  private org.apache.tomcat.InstanceManager _jsp_instancemanager;

  public java.util.Map<java.lang.String,java.lang.Long> getDependants() {
    return _jspx_dependants;
  }

  public void _jspInit() {
    _el_expressionfactory = _jspxFactory.getJspApplicationContext(getServletConfig().getServletContext()).getExpressionFactory();
    _jsp_instancemanager = org.apache.jasper.runtime.InstanceManagerFactory.getInstanceManager(getServletConfig());
  }

  public void _jspDestroy() {
  }

  public void _jspService(final javax.servlet.http.HttpServletRequest request, final javax.servlet.http.HttpServletResponse response)
        throws java.io.IOException, javax.servlet.ServletException {

    final javax.servlet.jsp.PageContext pageContext;
    javax.servlet.http.HttpSession session = null;
    final javax.servlet.ServletContext application;
    final javax.servlet.ServletConfig config;
    javax.servlet.jsp.JspWriter out = null;
    final java.lang.Object page = this;
    javax.servlet.jsp.JspWriter _jspx_out = null;
    javax.servlet.jsp.PageContext _jspx_page_context = null;


    try {
      response.setContentType("text/html");
      pageContext = _jspxFactory.getPageContext(this, request, response,
      			null, true, 8192, true);
      _jspx_page_context = pageContext;
      application = pageContext.getServletContext();
      config = pageContext.getServletConfig();
      session = pageContext.getSession();
      out = pageContext.getOut();
      _jspx_out = out;

      out.write("<!doctype html>\n");
      out.write("<html class=\"no-js\">\n");
      out.write("<head>\n");
      out.write("    <meta charset=\"utf-8\">\n");
      out.write("    <title></title>\n");
      out.write("    <meta name=\"description\" content=\"\">\n");
      out.write("    <meta name=\"viewport\" content=\"width=device-width\">\n");
      out.write("    <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->\n");
      out.write("    <!-- build:css(.) styles/vendor.css -->\n");
      out.write("    <!-- bower:css -->\n");
      out.write("    <link rel=\"stylesheet\" href=\"bower_components/bootstrap/dist/css/bootstrap.css\" />\n");
      out.write("    <link rel=\"stylesheet\" href=\"bower_components/AngularJS-Toaster/toaster.css\" />\n");
      out.write("    <!-- endbower -->\n");
      out.write("    <!-- endbuild -->\n");
      out.write("    <!-- build:css(.tmp) styles/main.css -->\n");
      out.write("    <link rel=\"stylesheet\" href=\"styles/main.css\">\n");
      out.write("    <!-- endbuild -->\n");
      out.write("</head>\n");
      out.write("<!-- \n");
      out.write("\tng-app is directive that declares that the element \n");
      out.write("\tand its children will be handled by angular.js \n");
      out.write("-->\n");
      out.write("<body ng-app=\"avAngularStartupApp\">\n");
      out.write("\t<!--[if lt IE 7]>\n");
      out.write("\t<p class=\"browsehappy\">You are using an <strong>outdated</strong> browser. Please <a href=\"http://browsehappy.com/\">upgrade\n");
      out.write("\t    your browser</a> to improve your experience.</p>\n");
      out.write("\t<![endif]-->\n");
      out.write("\t\n");
      out.write("\t<!-- Add your site or application content here -->\n");
      out.write("\t<div class=\"container\">\n");
      out.write("\t    <div class=\"header\">\n");
      out.write("\t        <ul class=\"nav nav-pills pull-right\">\n");
      out.write("\t            <li class=\"active\"><a ng-href=\"{{$root.pateka}}\">Home</a></li>\n");
      out.write("\t            <li><a ng-href=\"#\">About</a></li>\n");
      out.write("\t            <li><a ng-href=\"#\">Contact</a></li>\n");
      out.write("\t        </ul>\n");
      out.write("\t        <h3 class=\"text-muted\">avAngularStartup</h3>\n");
      out.write("\t    </div>\n");
      out.write("\t\n");
      out.write("\t    <div class=\"container\">\n");
      out.write("\t    \t<!-- \n");
      out.write("\t    \t\tng-view is directive that declares that the element will be \n");
      out.write("\t    \t\tplace holder for the partial files included through the router\n");
      out.write("\t    \t -->\n");
      out.write("\t        <div ng-view></div>\n");
      out.write("\t    </div>\n");
      out.write("\t\n");
      out.write("\t    <div class=\"footer\">\n");
      out.write("\t        <p><span class=\"glyphicon glyphicon-heart\"></span> from the Web programming team</p>\n");
      out.write("\t    </div>\n");
      out.write("\t</div>\n");
      out.write("\t\n");
      out.write("\t\n");
      out.write("\t\n");
      out.write("\t<!-- build:js(.) scripts/oldieshim.js -->\n");
      out.write("\t\t<!--[if lt IE 9]>\n");
      out.write("\t\t\t<script src=\"bower_components/es5-shim/es5-shim.js\"></script>\n");
      out.write("\t\t\t<script src=\"bower_components/json3/lib/json3.js\"></script>\n");
      out.write("\t\t<![endif]-->\n");
      out.write("\t<!-- endbuild -->\n");
      out.write("\t\n");
      out.write("\t<!-- build:js(.) scripts/vendor.js -->\n");
      out.write("\t<!-- bower:js -->\n");
      out.write("\t<script src=\"bower_components/jquery/dist/jquery.js\"></script>\n");
      out.write("\t<script src=\"bower_components/angular/angular.js\"></script>\n");
      out.write("\t<script src=\"bower_components/bootstrap/dist/js/bootstrap.js\"></script>\n");
      out.write("\t<script src=\"bower_components/angular-bootstrap/ui-bootstrap-tpls.js\"></script>\n");
      out.write("\t<script src=\"bower_components/angular-animate/angular-animate.js\"></script>\n");
      out.write("\t<script src=\"bower_components/AngularJS-Toaster/toaster.js\"></script>\n");
      out.write("\t<script src=\"bower_components/angular-route/angular-route.js\"></script>\n");
      out.write("\t<script src=\"bower_components/angular-resource/angular-resource.js\"></script>\n");
      out.write("\t<!-- endbower -->\n");
      out.write("\t<!-- endbuild -->\n");
      out.write("\t\n");
      out.write("\t<!-- These scripts hold the code of the application -->\n");
      out.write("\t<!-- build:js({.tmp,app}) scripts/scripts.js -->\n");
      out.write("\t<!-- The definition and the configuration of the application module -->\n");
      out.write("\t<script src=\"scripts/app.js\"></script>\n");
      out.write("\t<!-- The route configuration -->\n");
      out.write("\t<script src=\"scripts/router.js\"></script>\n");
      out.write("\t<!-- controllers definition -->\n");
      out.write("\t<script src=\"scripts/controllers/main.js\"></script>\n");
      out.write("\t<!-- Services definition -->\n");
      out.write("\t<script src=\"scripts/services/services.js\"></script>\n");
      out.write("\t<!-- endbuild -->\n");
      out.write("</body>\n");
      out.write("</html>\n");
    } catch (java.lang.Throwable t) {
      if (!(t instanceof javax.servlet.jsp.SkipPageException)){
        out = _jspx_out;
        if (out != null && out.getBufferSize() != 0)
          try { out.clearBuffer(); } catch (java.io.IOException e) {}
        if (_jspx_page_context != null) _jspx_page_context.handlePageException(t);
        else throw new ServletException(t);
      }
    } finally {
      _jspxFactory.releasePageContext(_jspx_page_context);
    }
  }
}
