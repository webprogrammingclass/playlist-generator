PlaylistgenApp.factory("videoService", ['$http', '$resource', function($http, $resource) {

	return {
		getPopularPlaylists: function() {
			return $http.get('api/videos/random');
		},
		youTubeVideos : function() {
			return $resource('api/videos/create/:q', {q : '@q'}, {
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}
			});
		},
		youTubeArtistVideos : function() {
			return $resource('api/videos/generate?genres=:genres&moods=:moods', {genres : '@genres', moods : '@moods'}, {
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}
			});
		}
	}
}]);