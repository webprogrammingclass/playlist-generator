PlaylistgenApp.factory("userService", ['$resource', function($resource) {
	return {
		setToken : function() {
			return $resource('users/:action', {}, {
				authenticate : {
					method : 'POST',
					params : {
						'action' : 'authenticate'
					},
					headers : {
						'Content-Type' : 'application/x-www-form-urlencoded'
					}
				}
			});
		},
		createUser : function() {
			return $resource('users/:action', {}, {
				create_user : {
					method : 'POST',
					params : {
						'action' : 'register'
					},
					headers : {
						'Content-Type' : 'application/x-www-form-urlencoded'
					}
				}
			});
		},
		users : function() {
			return $resource('users/:id', {id : '@id'}, {
				update : {
					method : 'PUT'
				},
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}
			});
		},
		usersGenresAndMoods : function() {
			return $resource('users/:id/tags', {id : '@id'}, {
				update : {
					method : 'PUT'
				},
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}
			});
		},
		usersPlaylists : function() {
			return $resource('users/:id/playlists', {id : '@id'}, {
				update : {
					method : 'PUT'
				},
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}
			});
		},
		usersFavoritePlaylists : function() {
			return $resource('users/:id/favorites', {id : '@id'}, {
				update : {
					method : 'PUT'
				},
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}
			});
		},
		userFavoritePlaylist : function() {
			return $resource('users/:id/favorites/:playlistID', {id : '@id', playlistID : '@playlistID'}, {
				update : {
					method : 'PUT'
				},
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}
			});
		},
		userRatedPlaylist : function() {
			return $resource('users/:id/rated/:playlistID', {id : '@id', playlistID : '@playlistID'}, {
				update : {
					method : 'PUT'
				},
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}
			});
		},
		usersRatedPlaylists : function() {
			return $resource('users/:id/rated', {id : '@id'}, {
				update : {
					method : 'PUT'
				},
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}
			});
		}
	}
}]);

PlaylistgenApp.factory("tokenService", ['$http', function($http) {
	return {
		getUserToken: function() {
			return $http.get("users/get_user_token");
		},
		deleteUserToken: function() {
			return $http.post("users/delete_user_token");
		}
	}
}]);