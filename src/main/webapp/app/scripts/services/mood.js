PlaylistgenApp.factory("moodService", ['$resource', '$http', function($resource, $http) {
	return {
		getMoodsFromAPI : function() {
			return $http.get('http://developer.echonest.com/api/v4/artist/list_terms?api_key=SL6Y54LFM7CFILKOS&format=json&type=mood');
		},
		moods : function() {
			return $resource('api/moods/:id', {id : '@id'}, {
				upadte : {
					method : 'PUT'
				},
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}
			});
		}
	}
}]);