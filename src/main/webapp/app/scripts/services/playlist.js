PlaylistgenApp.factory("playlistService", ['$resource', function($resource) {

	return {
		showPlaylists : function() {
			return $resource('api/playlists/all/:param/:userID?page=:page&size=:size', {param : '@param', userID : '@userID', page : '@page', size : '@size'}, {
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}
			});
		},
		showAllPlaylists: function() {
			return $resource('api/playlists/all/?page=:page&size=:size', {page : '@page', size : '@size'}, {
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}
			});
		},
		playlists : function() {
			return $resource('api/playlists/:id', {id : '@id'}, {
				update : {
					method : 'PUT'
				},
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}
			});
		},
		generatePlaylist : function() {
			return $resource('api/playlists/generate', {id : '@id'}, {
				update : {
					method : 'PUT'
				},
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}
			});
		},
		newestPlaylists : function() {
			return $resource('api/playlists/newest',{}, {
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}
			});
		},
		topRatedPlaylists : function() {
			return $resource('api/playlists/top-rated',{}, {
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}
			});
		},
		searchPlaylists : function() {
			return $resource('api/playlists/search?q=:q&page=:page&size=:size', {q : '@q', page : '@page', size : '@size'}, {
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}
			});
		}
	}
}]);