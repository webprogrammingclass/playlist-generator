PlaylistgenApp.factory("genreService", ['$resource', '$http', function($resource, $http) {
	return {
		getGenresFromAPI : function() {
			return $http.get('http://developer.echonest.com/api/v4/artist/list_terms?api_key=SL6Y54LFM7CFILKOS&format=json&type=style');
		},
		genres : function() {
			return $resource('api/genres/:id', {id : '@id'}, {
				upadte : {
					method : 'PUT'
				},
				headers : {
					'Content-Type' : 'application/x-www-form-urlencoded'
				}
			});
		}
	}
}]);