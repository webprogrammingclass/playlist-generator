/**
 * @ngdoc here we are configuring the module exposed through the FirstApp
 *        variable. The method receives an array that has a function as a last
 *        argument. Here, the angular inject the dependencies defined as strings
 *        in the array to the corresponding elements in the function. <br/> The
 *        $routeProvider is used to configure the routes. It maps templateUrl
 *        and optionally a controller to a given path. This is used by the
 *        ng-view directive. It replaces the content of the defining element
 *        with the content of the templateUrl, and connects it to the controller
 *        through the $scope.
 * @see https://docs.angularjs.org/guide/di
 */
PlaylistgenApp.config(['$stateProvider', '$urlRouterProvider', function($stateProvider, $urlRouterProvider) {
	
	$stateProvider.state('root', {
		url : '',
		abstract: true,
		views : {
    	  "header" : { 
    		  templateUrl : "views/templates/header.html", 
    		  controller : "HeaderCtrl", 
    	  },
    	  "footer" : {
    		  templateUrl : "views/templates/footer.html"
    	  }
      }
    });
	
	$stateProvider.state('root.main', {
		url : '/',
		views : {
			'container@' : {
				templateUrl : "views/main.html",
		    	controller : "MainCtrl" 
			}
		}
	});
	
	$stateProvider.state('root.admin', {
		url : "/admin",
		views : {
			"container@" : { 
				templateUrl : "views/admin.html", 
				controller : "AdminCtrl"
			}
		}
	});
	
	$stateProvider.state('root.profile', {
		url : "/profile",
		views : {
			"container@" : { 
				templateUrl : "views/profile.html", 
				controller : "ProfileCtrl"
			}
		}
	});
	
	$stateProvider.state('root.search', {
		url : "/search",
		views : {
			"container@" : { 
				templateUrl : "views/search.html", 
				controller : "SearchCtrl"
			}
		}
	});
	
	$stateProvider.state('root.generatePlaylist', {
		url : "/generate-playlist",
		views : {
			"container@" : { 
				templateUrl : "views/generate-playlist.html", 
				controller : "GeneratePlaylistCtrl"
			}
		}
	});
	
	$stateProvider.state('root.playlists', {
		url : "/playlists",
		views : {
			"container@" : { 
				templateUrl : "views/playlists.html", 
				controller : "PlaylistsCtrl"
			}
		}
	});
	
	$stateProvider.state('root.playlist', {
		url : "/playlists/{id}",
		views : {
			"container@" : { 
				templateUrl : "views/playlist.html", 
				controller : "PlaylistCtrl"
			}
		}
	});
	
	$stateProvider.state('error', {
		url : "/404",
		templateUrl: '404.html'
	});
	  
	$urlRouterProvider.otherwise('/');
}]);
