'use strict';

//Header Controller
PlaylistgenApp.controller('HeaderCtrl', ['$scope', 'ngDialog', '$window', 'tokenService', '$location', function($scope, ngDialog, $window, tokenService, $location) {
	$scope.isAuthenticated = false;
	$scope.isAdmin = false;
	
	$scope.userID = "";
	$scope.username = "";
	$scope.name = "";
	$scope.surname = "";
	$scope.email = "";
	
	$scope.activeHome = [];
	$scope.activeSearch = [];
	$scope.activePlaylists = [];
	$scope.activeGenerate = [];
	$scope.activeCtrlPanel = [];
	
	//dodadi active klasa
	$scope.activeHome.push('active');
	
	$scope.activateHome = function() {
		$scope.activeHome.push('active');
		$scope.activeSearch.pop('active');
		$scope.activeCtrlPanel.pop('active');
		$scope.activeGenerate.pop('active');
		$scope.activePlaylists.pop('active');
	}
	
	$scope.activateSearch = function() {
		$scope.activeSearch.push('active');
		$scope.activeHome.pop('active');
		$scope.activeCtrlPanel.pop('active');
		$scope.activeGenerate.pop('active');
		$scope.activePlaylists.pop('active');
	}
	
	$scope.activateCtrlPanel = function() {
		$scope.activeCtrlPanel.push('active');
		$scope.activeHome.pop('active');
		$scope.activeSearch.pop('active');
		$scope.activeGenerate.pop('active');
		$scope.activePlaylists.pop('active');
	}
	
	$scope.activateGenerate = function() {
		$scope.activeGenerate.push('active');
		$scope.activeHome.pop('active');
		$scope.activeSearch.pop('active');
		$scope.activeCtrlPanel.pop('active');
		$scope.activePlaylists.pop('active');
	}
	
	$scope.activatePlaylists = function() {
		$scope.activePlaylists.push('active');
		$scope.activeHome.pop('active');
		$scope.activeSearch.pop('active');
		$scope.activeCtrlPanel.pop('active');
		$scope.activeGenerate.pop('active');
	}
	
	tokenService.getUserToken().then(function(response) {
		var userToken = response.data.token;
		
		if(userToken != "NOT LOGED IN") {
			//korisnikot e najaven
			userToken = JSON.parse(userToken);
			
			var roles = userToken.roles;
			var username = userToken.username;
			var userInfo = JSON.parse(response.data.userInfo);
			
			var flag = false;
			for(var i=0; i<roles.length; i++) {
				if(roles[i] == "ROLE_ADMIN") {
					flag = true;
					$scope.isAdmin = true;
				}
				else if(roles[i] == "ROLE_USERS")
					flag = true;
			}
			
			if(!flag) {
				//korisnikot nema pravo da pristapi na home stranicata
				
			}
			else {
				$scope.isAuthenticated = true;
				
				$scope.userID = userInfo.userID;
				$scope.username = username;
				$scope.name = userInfo.name;
				$scope.surname = userInfo.surname;
				$scope.email = userInfo.email;
			}
		}
		else {
			//korisnikot ne e najaven
			
		}
	});
	
	$scope.openModal = function() {
		console.log("Vlez");
		var new_dialog = ngDialog.open({ templateUrl: 'views/templates/login.html', controller: 'LoginCtrl', scope: $scope });
	}
	
	$scope.openModalToChangePass = function() {
		var new_dialog = ngDialog.open({ templateUrl: 'views/templates/changePassword.html', controller: 'ChangePassCtrl', scope: $scope });
	}
	
	$scope.logout = function() {
		tokenService.deleteUserToken().then(function(response) {
			$window.location.reload();
		}, function(error) {
			console.log(error);
		});
	}
}]);