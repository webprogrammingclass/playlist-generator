'use strict';

//Admin Controller
PlaylistgenApp.controller('AdminCtrl', ['$scope', '$rootScope', '$location', 'tokenService', '$window', 'playlistService', 'ngDialog', 'moodService', 'genreService', function($scope, $rootScope, $location, tokenService, $window, playlistService, ngDialog, moodService, genreService) {
	$scope.myPlaylistClass = [];
	$scope.usersPlaylistClass = [];
	$scope.createPlaylistClass = [];
	
	$scope.userID = "";
	$scope.name = "";
	$scope.surname = "";
	$scope.image = "";
	
	$scope.page = {
		totalItems : 0,
		size : 5,
		currentPage : 0
	};
	
	$scope.playlists = [];
	$scope.currentPlaylists = "adminplaylists";
	$scope.hasUser = false;
	
	$scope.slickConfig = {
		autoplay: false,
		draggable: true,
		dots: false,
		method: {},
		event: {
			beforeChange: function (event, slick, currentSlide, nextSlide) {
			},
		    afterChange: function (event, slick, currentSlide, nextSlide) {
		    }
		}
	};
	
	tokenService.getUserToken().then(function(response) {
		var userToken = response.data.token;
		
		if(userToken == "NOT LOGED IN") {
			//korisnikot ne e najaven!
			$location.path("/");
		}
		else {
			userToken = JSON.parse(userToken);
			
			var userInfo = JSON.parse(response.data.userInfo);
			var roles = userToken.roles;
			
			var flag = false;
			for(var i=0; i<roles.length; i++)
				if(roles[i] == "ROLE_ADMIN") {
					flag = true;
					$scope.isAdmin = true;
				}
			if(!flag) {
				//korisnikot nema pravo da pristapi na /admin stranicata
				$location.path("/");
			}
			
			$scope.userID = userInfo.userID;
			$scope.name = userInfo.name;
			$scope.surname = userInfo.surname;
			$scope.image = userInfo.image;
			
			playlistService.showPlaylists().get({param : $scope.currentPlaylists, userID : $scope.userID, page : $scope.page.currentPage, size : $scope.page.size}, function(response) {
				$scope.page.currentPage = response.pagable.page;
				$scope.page.totalItems = response.pagable.totalElements;
				$scope.playlists = response.playlists;
				$scope.hasUser = false;
			});
		}
	});
	
	genreService.genres().get({}, function(response) {
		$scope.isAllGenresIntoDatabase = (response.genres.length > 999) ? true : false;
	});
	
	moodService.moods().get({}, function(response) {
		$scope.isAllMoodsIntoDatabase = (response.moods.length > 70) ? true : false;
	});
	
	$scope.showAdminPlaylists = function() {
		$scope.myPlaylistClass.push('active');
		$scope.usersPlaylistClass.pop('active');
		$scope.createPlaylistClass.pop('active');
		
		$scope.currentPlaylists = "adminplaylists";
		$scope.page.currentPage = 0;
		
		playlistService.showPlaylists().get({param : $scope.currentPlaylists, userID : $scope.userID, page : $scope.page.currentPage, size : $scope.page.size}, function(response) {
			$scope.page.currentPage = response.pagable.page;
			$scope.page.totalItems = response.pagable.totalElements;
			$scope.playlists = response.playlists;
			$scope.hasUser = false;
		});
	}
	
	$scope.showUsersPlaylists = function() {
		$scope.usersPlaylistClass.push('active');
		$scope.myPlaylistClass.pop('active');
		$scope.createPlaylistClass.pop('active');
		
		$scope.currentPlaylists = "usersplaylists";
		$scope.page.currentPage = 0;
		
		playlistService.showPlaylists().get({param : $scope.currentPlaylists, userID : $scope.userID, page : $scope.page.currentPage, size : $scope.page.size}, function(response) {
			$scope.page.currentPage = response.pagable.page;
			$scope.page.totalItems = response.pagable.totalElements;
			$scope.playlists = response.playlists;
			$scope.hasUser = true;
		});
	}
	
	$scope.addAllGenres = function() {
		genreService.getGenresFromAPI().then(function(response) {
			var genres = response.data.response.terms;
			
			genreService.genres().save(genres, function(response) {
				console.log(response.data);
				if(response.data == "OK")
					alert("OK");
				else
					alert("Can not add all genres into database!");
			});
		}, function(error) {
			swal({
				title: "Error",
			    text: response.error,
			    type: "error",
			    showCancelButton: false,
			    confirmButtonClass: 'btn-danger',
			    confirmButtonText: 'Close'
			});
		});
	}
	
	$scope.addAllMoods = function() {
		moodService.getMoodsFromAPI().then(function(response) {
			var moods = response.data.response.terms;
			
			moodService.moods().save(moods, function(response) {
				console.log(response.data);
				if(response.data == "OK")
					alert("OK");
				else
					alert("Can not add all moods into database!");
			});
		}, function(error) {
			swal({
				title: "Error",
			    text: response.error,
			    type: "error",
			    showCancelButton: false,
			    confirmButtonClass: 'btn-danger',
			    confirmButtonText: 'Close'
			});
		});
	}
	
	$scope.openModal = function() {
		var new_dialog = ngDialog.open({ templateUrl: "views/templates/createPlaylist.html", controller: 'CreatePlaylistCtrl', scope: $scope, className: 'ngdialog-theme-mine' });
	}
	
	$scope.editPlaylistt = function(playlist) {
		playlistService.playlists().get({id : playlist.playlistID}, function(response) {
			$scope.editPlaylist = response;
			console.log(response);
			var edit_dialog = ngDialog.open({ templateUrl : "views/templates/editPlaylist.html", controller: 'EditPlaylistCtrl', scope: $scope, className: 'ngdialog-theme-mine' });
		});
	}
	
	$scope.deletePlaylist = function(playlist, $event) {
		var deletePlaylist = {
			playlistID : playlist.playlistID,
			name : playlist.name,
			date : playlist.date
		};
		
		$scope.delPlaylist = deletePlaylist;
		
		var delete_dialog = ngDialog.open({ templateUrl: "views/templates/deletePlaylist.html", controller: 'DeletePlaylistCtrl', scope: $scope });
	}
	
	$scope.pageChanged = function () {
        playlistService.showPlaylists().get({param : $scope.currentPlaylists, userID : $scope.userID, page: $scope.page.currentPage - 1, size: $scope.page.size}, function(response) {
			$scope.playlists = response.playlists;
		});
    };
}]);