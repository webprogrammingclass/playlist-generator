'use strict';

//DeletePlaylist Controller
PlaylistgenApp.controller('DeletePlaylistCtrl', ['$scope', 'playlistService', '$window', function($scope, playlistService, $window) {
	$scope.btnText = "Delete";
	$scope.btnClass = [];
	
	$scope.deletePlaylist = function() {
		$scope.btnText = "Deleting...";
		$scope.btnClass.push('glyphicon glyphicon-refresh glyphicon-refresh-animate');
		
		playlistService.playlists().remove({id : $scope.delPlaylist.playlistID}, function(response) {
			if(response.data == "OK")
				$window.location.reload();
			else
				{
					swal({
						title: "Error",
					    text: "Error while deleting the playlist",
					    type: "error",
					    showCancelButton: false,
					    confirmButtonClass: 'btn-danger',
					    confirmButtonText: 'Close'
					});
				}
		}, function(error) {
			swal({
				title: "Error",
			    text: error,
			    type: "error",
			    showCancelButton: false,
			    confirmButtonClass: 'btn-danger',
			    confirmButtonText: 'Close'
			});
		});
	}
}]);