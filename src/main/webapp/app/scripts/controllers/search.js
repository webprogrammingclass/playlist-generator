'use strict';

//Search Controller
PlaylistgenApp.controller('SearchCtrl', ['$scope', '$rootScope', '$stateParams','$window', 'playlistService', 'genreService', 'moodService', '$sce', function($scope, $rootScope, $stateParams, $window, playlistService, videoService, genreService, moodService, $sce) {
	$scope.wellHeight = [];
	$scope.wellHeight.push('addHeigth');
	
	$scope.playlists = [];
	
	$scope.page = {
		totalItems : 0,
		size : 5,
		currentPage : 0
	};
	
	$scope.searchPlaylists = function() {
		console.log($rootScope.playlists);
		if($rootScope.playlists && $rootScope.page) {
			$scope.playlists = $rootScope.playlists;
			$scope.page.currentPage = $rootScope.page.currentPage;
			$scope.page.totalItems = $rootScope.page.totalItems;
		}
		if($scope.query != null && $scope.query != "") {
			playlistService.searchPlaylists().get({q : $scope.query, page: $scope.page.currentPage - 1, size: $scope.page.size}, function(response) {
				$scope.playlists = response.playlists;
				$scope.page.currentPage = response.pagable.page;
				$scope.page.totalItems = response.pagable.totalElements;
			});
		}
		else
			alert('Please enter some query!');
	}
	
	$scope.pageChanged = function () {
		playlistService.searchPlaylists().get({q : $scope.query, page: $scope.page.currentPage - 1, size: $scope.page.size}, function(response) {
			$scope.playlists = response.playlists;
		});
    };
}]);
