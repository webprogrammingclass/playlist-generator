'use strict';

//Playlist Controller
PlaylistgenApp.controller('PlaylistCtrl', ['$scope', '$stateParams', 'tokenService', 'userService', 'playlistService', '$sce', function($scope, $stateParams, tokenService, userService, playlistService, $sce) {
	$scope.btnText = "";
	$scope.btnClass = [];
	$scope.btnClass2 = [];
	
	$scope.isAuthenticated = false;
	
	$scope.playlistID = $stateParams.id;
	$scope.userID = "";
	
	tokenService.getUserToken().then(function(response) {
		var userToken = response.data.token;
		
		if(userToken != "NOT LOGED IN") {
			//korisnikot e najaven
			userToken = JSON.parse(userToken);
			
			var roles = userToken.roles;
			var userInfo = JSON.parse(response.data.userInfo);
			
			for(var i=0; i<roles.length; i++) {
				if(roles[i] == "ROLE_ADMIN") {
					$scope.isAuthenticated = true;
					$scope.isAdmin = true;
				}
				else if(roles[i] == "ROLE_USERS")
					$scope.isAuthenticated = true;
			}
			
			$scope.userID = userInfo.userID;
			
			userService.userFavoritePlaylist().get({id : $scope.userID, playlistID : $scope.playlistID}, function(response) {
				var message = response.message;
				if(message == "NO") {
					$scope.btnText = "Add To Favorites";
					if($scope.btnClass.lenght > 0) {
						$scope.btnClass.pop('btn btn-sm btn-danger');
						$scope.btnClass.pop('btnRemove');
					}
					$scope.btnClass.push('btn btn-sm btn-success');
					$scope.btnClass.push('btnAdd');
				}
				else {
					$scope.btnText = "Delete From Favorites";
					if($scope.btnClass.lenght > 0) {
						$scope.btnClass.pop('btn btn-sm btn-success');
						$scope.btnClass.pop('btnAdd');
					}
					$scope.btnClass.push('btn btn-sm btn-danger');
					$scope.btnClass.push('btnRemove');
				}
			});
			
			userService.userRatedPlaylist().get({id : $scope.userID, playlistID : $scope.playlistID}, function(response) {
				var message = response.message;
				if(message == "NO")
					$scope.btnClass2.pop('cantRate');
				else 
					$scope.btnClass2.push('cantRate');
			});
		}
		else {
			//korisnikot ne e najaven
			$scope.btnClass2.push('cantRate')
		}
	});
	
	playlistService.playlists().get({id : $scope.playlistID}, function(response) {
		$scope.playlist = response;
		
		for(var i=0; i<$scope.playlist.videos.length; i++)
			$scope.playlist.videos[i].embed = $sce.trustAsResourceUrl("http://www.youtube.com/embed/" + $scope.playlist.videos[i].url);
		$scope.rating = $scope.playlist.rating;
	});
	
	$scope.submitFavorites = function() {
		if($scope.btnText == "Add To Favorites") {
			userService.userFavoritePlaylist().save({id : $scope.userID, playlistID : $scope.playlistID}, function(response) {
				$scope.btnText = "Delete From Favorites";
				
				$scope.btnClass.pop('btn btn-sm btn-success');
				$scope.btnClass.pop('btnAdd');
				$scope.btnClass.push('btnRemove');
				$scope.btnClass.push('btn btn-sm btn-danger');
			});
		}
		else {
			userService.userFavoritePlaylist().remove({id : $scope.userID, playlistID : $scope.playlistID}, function(response) {
				if(response.message == "YES") {
					$scope.btnText = "Add To Favorites";
					
					$scope.btnClass.pop('btn btn-sm btn-danger');
					$scope.btnClass.pop('btnRemove');
					$scope.btnClass.push('btnAdd');
					$scope.btnClass.push('btn btn-sm btn-success');
				}
			});
		}
	}
	
	$scope.rateFunction = function(rating) {
		var data = {
			rating : rating
		};
		
		userService.userRatedPlaylist().save({id : $scope.userID, playlistID : $scope.playlistID}, data, function(response) {
			$scope.rating = rating;
			if(response.message == "YES")
 				$scope.btnClass2.push('cantRate');
			swal({
				title: "Success",
			    text: "You have rated " + $scope.playlist.name + " with " + $scope.rating,
			    type: "success",
			    showCancelButton: false,
			    showConfirmButton: false
			});
		});
	};
}]);