'use strict';

//Login Controller
PlaylistgenApp.controller('LoginCtrl', ['$scope', '$rootScope', '$location', '$window', 'userService', 'tokenService', function($scope, $rootScope, $location, $window, userService, tokenService) {

	$scope.rememberMe = false;
	$scope.isSignIn = false;
	
	$scope.loginClass = [];
	$scope.registerClass = [];
	
	//dodadi active klasa za signIn
	$scope.loginClass.push('active');
	
	$scope.signIn = function() {
		$scope.isSignIn = false;
		
		$scope.loginClass.push('active');
		$scope.registerClass.pop('active');
	}
	
	$scope.register = function() {
		$scope.isSignIn = true;
		
		$scope.loginClass.pop('active');
		$scope.registerClass.push('active');
	}
	
	$scope.login = function() {
		if($scope.username != "" && $scope.password != "") {
			userService.setToken().authenticate($.param({ username : $scope.username, password : $scope.password, rememberMe : $scope.rememberMe }), function(authenticationResult) {
				if(authenticationResult.token == "Invalid password or username!")
					alert(authenticationResult.token);
				else {
				var json = JSON.parse(authenticationResult.token);
				var roles = json.roles;
				var flag = false;
				
				for(var i=0; i<roles.length; i++) {
					if(roles[i] == "ROLE_ADMIN" || roles[i] == "ROLE_USERS") {
						flag = true;
						$window.location.reload();
						break;
					}
				}
				
				if(!flag)
					alert("You can not enter to this page!");
				else
					$scope.closeThisDialog('true');
				}
			}, function(error) {
				swal({
					title: "Error",
				    text: error.statusText + ". Invalid password or username!",
				    type: "warning",
				    showCancelButton: false,
				    confirmButtonClass: 'btn-warning',
				    confirmButtonText: 'Close'
				});
			});
		}
		else {
			alert("You must enter all field correctly!");
		}
	}
	
	$scope.signup = function() {
		if($scope.name == null || $scope.lastname == null || $scope.regUsername == null || $scope.regPassword == null || $scope.repassword == null || $scope.email == null || $scope.age == null || $scope.sex == null || $scope.regPassword != $scope.repassword)
			alert("You must enter all field correctly!");
		else {
			userService.createUser().create_user($.param({ name : $scope.name, lastname : $scope.lastname, username : $scope.regUsername, password : $scope.regPassword, repassword : $scope.repassword, email : $scope.email, age : $scope.age, sex : $scope.sex }), function(response) {
				console.log(response.token.indexOf("There is an account with"));
				if(response.token.indexOf("There is an account with") == -1) {
					console.log(response.token);
					var json = JSON.parse(response.token);
					var roles = json.roles;
					var flag = false;
					
					for(var i=0; i<roles.length; i++) {
						if(roles[i] == "ROLE_ADMIN" || roles[i] == "ROLE_USERS") {
							flag = true;
							$window.location.reload();
							break;
						}
					}
					
					if(!flag)
						alert("You can not enter to this page!");
					else
						$scope.closeThisDialog('true');
				}
				else
					alert(response.token);
			});
		}
	}
}]);