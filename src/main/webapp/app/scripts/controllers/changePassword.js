'use strict';

//ChangePassword Controller
PlaylistgenApp.controller('ChangePassCtrl', ['$scope', 'userService', '$window', function($scope, userService, $window) {
	
	$scope.changePassword = function() {
		if($scope.password != null && $scope.password != "" && $scope.newPassword != null && $scope.newPassword != "" && $scope.newPasswordConf != null && $scope.newPasswordConf != "") {
			if($scope.newPassword == $scope.newPasswordConf) {
				var passwords = {
					oldPassword : $scope.password,
					newPassword : $scope.newPassword,
					newPasswordConf : $scope.newPasswordConf,
					flag : true
				};
				
				userService.users().update({id : $scope.userID}, passwords, function(response) {
					
					var json = response;
					if(json.error)
					{	
						swal({
							title: "Error",
						    text: response.error,
						    type: "error",
						    showCancelButton: false,
						    confirmButtonClass: 'btn-danger',
						    confirmButtonText: 'Close'
						});
					}
					else
						{
							swal({
								title: "Success",
							    text: "You successfully change your password",
							    type: "success",
							    showCancelButton: false,
							    showConfirmButton: false
							});
							$window.location.reload();
						}
				});
			}
			else
				{
					swal({
						title: "Error",
					    text: "Your new password must be same with confirm password!",
					    type: "warning",
					    showCancelButton: false,
					    confirmButtonClass: 'btn-warning',
					    confirmButtonText: 'Close'
					});
				}
		}
		else
			{
				swal({
					title: "Error",
				    text: "Please fill all the fields to change your password!",
				    type: "warning",
				    showCancelButton: false,
				    confirmButtonClass: 'btn-warning',
				    confirmButtonText: 'Close'
				});
			}
	}
}]);
