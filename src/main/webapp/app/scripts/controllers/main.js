'use strict';

//Main Controller
PlaylistgenApp.controller('MainCtrl', ['$scope', '$rootScope', '$state', 'playlistService', '$sce', 'tokenService', '$location', function($scope, $rootScope, $state, playlistService, $sce, tokenService, $location) {
	$scope.isAuthenticated = false;
	$scope.isAdmin = false;
	
	$scope.page = {
			totalItems : 0,
			size : 5,
			currentPage : 0
		};
	
	tokenService.getUserToken().then(function(response) {
		var userToken = response.data.token;
		
		if(userToken != "NOT LOGED IN") {
			//korisnikot e najaven
			userToken = JSON.parse(userToken);
			
			var roles = userToken.roles;
			var flag = false;
			
			for(var i=0; i<roles.length; i++) {
				if(roles[i] == "ROLE_ADMIN") {
					flag = true;
					$scope.isAdmin = true;
				}
				else if(roles[i] == "ROLE_USERS")
					flag = true;
			}
			
			$scope.isAuthenticated = flag;
		}
		else {
			//korisnikot ne e najaven
			
		}
	});
	
	playlistService.newestPlaylists().get({}, function(response) {
		$scope.newestPlaylists = response.playlists;
	});
	
	playlistService.topRatedPlaylists().get({}, function(response) {
		$scope.topRatedPlaylists = response.playlists;
	});
	
	$scope.searchPlaylists = function() {
		if($scope.query != "") {
			playlistService.searchPlaylists().get({q : $scope.query, page: $scope.page.currentPage - 1, size: $scope.page.size}, function(response) {
				
				$rootScope.playlists = response.playlists;
				console.log($rootScope.playlists);
				$rootScope.page = {};
				$rootScope.page.currentPage = response.pagable.page;
				$rootScope.page.totalItems = response.pagable.totalElements;
				
				$state.go('root.search');
			});
		}
		else
			alert('Please enter some query!');
	};
}]);