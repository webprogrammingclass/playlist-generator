'use strict';

//Playlists Controller
PlaylistgenApp.controller('PlaylistsCtrl', ['$scope', '$window', 'playlistService', '$sce', '$timeout', '$location', function($scope, $window, playlistService, $sce, $timeout, $location) {
	
	$scope.page = {
		totalItems : 0,
		size : 10,
		currentPage : 0
	};
	
	$scope.playlists = [];
	
	playlistService.showAllPlaylists().get({page : $scope.page.currentPage, size : $scope.page.size}, function(response) {
		$scope.page.currentPage = response.pagable.page;
		$scope.page.totalItems = response.pagable.totalElements;
		$scope.playlists = response.playlists;
	});
	
	$scope.pageChanged = function () {
        playlistService.showAllPlaylists().get({page: $scope.page.currentPage - 1, size: $scope.page.size}, function(response) {
			$scope.playlists = response.playlists;
		});
    };
}]);