'use strict';

//Profile Controller
PlaylistgenApp.controller('ProfileCtrl', ['$scope', 'tokenService', 'userService', 'playlistService', 'genreService', 'moodService','$timeout', function($scope, tokenService, userService, playlistService, genreService, moodService, $timeout) {
	$scope.isAdmin = false;
	$scope.isAuthenticated = false;
	
	$scope.userID = "";
	$scope.name = "";
	$scope.surname = "";
	$scope.email = "";
	$scope.image = "";
	
	$scope.page = {
		totalItems : 0,
		size : 5,
		currentPage : 0
	};
	
	$scope.genres = [];
	$scope.moods = [];
	$scope.playlists = [];
	$scope.currentPlaylists = "userplaylists";
	
	$scope.slickConfig = {
		autoplay: false,
		draggable: true,
		dots: false,
		method: {},
		event: {
			beforeChange: function (event, slick, currentSlide, nextSlide) {
			},
			afterChange: function (event, slick, currentSlide, nextSlide) {
			}
		}
	};
	
	tokenService.getUserToken().then(function(response) {
		var userToken = response.data.token;
		
		if(userToken != "NOT LOGED IN") {
			//korisnikot e najaven
			userToken = JSON.parse(userToken);
			
			var userInfo = JSON.parse(response.data.userInfo);
			var roles = userToken.roles;
			
			for(var i=0; i<roles.length; i++) {
				if(roles[i] == "ROLE_ADMIN") {
					$scope.isAuthenticated = true;
					$scope.isAdmin = true;
				}
				else if(roles[i] == "ROLE_USERS")
					$scope.isAuthenticated = true;
			}
			
			if(!$scope.isAuthenticated) {
				//korisnikot nema pravo da pristapi na /profile stranicata
				$location.path("/");
			}
			
			$scope.userID = userInfo.userID;
			$scope.name = userInfo.name;
			$scope.surname = userInfo.surname;
			$scope.email = userInfo.email;
			$scope.image = userInfo.image;
			
			userService.usersGenresAndMoods().get({id : $scope.userID}, function(response) {
				$scope.genres = response.userGenres;
				$scope.moods = response.userMoods;
				
				userService.usersPlaylists().get({id : $scope.userID, page : $scope.page.currentPage, size : $scope.page.size}, function(response) {
					$scope.page.currentPage = response.pageable.page;
					$scope.page.totalItems = response.pageable.totalElements;
					$scope.playlists = response.userPlaylists;
					
					addMagnificPopup($scope.playlists);
				});
			});
		}
		else {
			//korisnikot ne e najaven
		}
	});
	
	$scope.showUserPlaylists = function() {
		$scope.currentPlaylists = "userplaylists";
		
		userService.usersPlaylists().get({id : $scope.userID, page : $scope.page.currentPage, size : $scope.page.size}, function(response) {
			$scope.page.currentPage = response.pageable.page;
			$scope.page.totalItems = response.pageable.totalElements;
			$scope.playlists = response.userPlaylists;
			
			addMagnificPopup($scope.playlists);
		});
	}
	
	$scope.showFavoritePlaylists = function() {
		$scope.currentPlaylists = "favoriteplaylists";
		
		userService.usersFavoritePlaylists().get({id : $scope.userID, page : $scope.page.currentPage, size : $scope.page.size}, function(response) {
			$scope.page.currentPage = response.pageable.page;
			$scope.page.totalItems = response.pageable.totalElements;
			$scope.playlists = response.userFavoritePlaylists;
			
			addMagnificPopup($scope.playlists);
		});
	}
	
	$scope.showRatedPlaylists = function() {
		$scope.currentPlaylists = "ratedplaylists";
		
		userService.usersRatedPlaylists().get({id : $scope.userID, page : $scope.page.currentPage, size : $scope.page.size}, function(response) {
			$scope.page.currentPage = response.pageable.page;
			$scope.page.totalItems = response.pageable.totalElements;
			$scope.playlists = response.userRatedPlaylists;
			
			addMagnificPopup($scope.playlists);
		});
	}
	
	$scope.pageChanged = function () {
		if($scope.currentPlaylists == "userplaylists") {
			userService.usersPlaylists().get({id : $scope.userID, page: $scope.page.currentPage - 1, size: $scope.page.size}, function(response) {
				$scope.playlists = response.userPlaylists;
				
				addMagnificPopup($scope.playlists);
			});
		}
		else if($scope.currentPlaylists == "favoriteplaylists") {
			userService.usersFavoritePlaylists().get({id : $scope.userID, page: $scope.page.currentPage - 1, size: $scope.page.size}, function(response) {
				$scope.playlists = response.userFavoritePlaylists;
				
				addMagnificPopup($scope.playlists);
			});
		}
		else {
			userService.usersRatedPlaylists().get({id : $scope.userID, page: $scope.page.currentPage - 1, size: $scope.page.size}, function(response) {
				$scope.playlists = response.userRatedPlaylists;
				
				addMagnificPopup($scope.playlists);
			});
		}
    }
	
	var addMagnificPopup = function(playlists) {
		for(var i=0; i<playlists.length; i++) {
			var playlist = playlists[i];
			for(var j=0; j<playlist.videos.length; j++)
				playlist.videos[j].embed = "http://www.youtube.com/watch?v=" + playlist.videos[j].url;
			
			$scope.$watch("playlist.videos", function (newValue, oldValue) {
				$timeout(function() {
					$('.gallery').magnificPopup({
						type: 'iframe',
						iframe: {
							markup: '<div class="mfp-iframe-scaler">' +
							'<div class="mfp-close"></div>' +
							'<iframe style="background: #fff none repeat scroll 0 0 !important" class="mfp-iframe" frameborder="0" allowfullscreen></iframe>' +
							'</div>', 
							patterns: {
								youtube: {
								      index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).

								      id: 'v=', // String that splits URL in a two parts, second part should be %id%
								      // Or null - full URL will be returned
								      // Or a function that should return %id%, for example:
								      // id: function(url) { return 'parsed id'; } 

								      src: '//www.youtube.com/embed/%id%?autoplay=1' // URL that will be set as a source for iframe. 
								    },
			                }
						},
						delegate: '.videos',
							gallery: {
							enabled: true
						}
					});
				});
			});
		}
	}
}]);
