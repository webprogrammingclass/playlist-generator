'use strict';

//GeneratePlaylist Controller
PlaylistgenApp.controller('GeneratePlaylistCtrl', ['$scope', '$window', 'tokenService', 'playlistService', 'videoService', 'genreService', 'moodService', '$sce', '$timeout','$location', function($scope, $window, tokenService, playlistService, videoService, genreService, moodService, $sce, $timeout,$location) {
	$scope.isAdmin = false;
	$scope.isAuthenticated = false;
	
	$scope.userID = "";
	$scope.name = "";
	$scope.surname = "";
	$scope.email = "";
	$scope.image = "";
	
	//Genres
	$scope.isGenreDropdownDisabled = false;
	$scope.selectedGenre = null;
	$scope.listOfSelectedGenres = [];
	
	//Moods
	$scope.isMoodDropdownDisabled = false;
	$scope.selectedMood = null;
	$scope.listOfSelectedMoods = [];
	
	$scope.playlistObj = {name : ""};
	$scope.videos = {};
	$scope.showBtn = true;
	
	tokenService.getUserToken().then(function(response) {
		var userToken = response.data.token;
		
		if(userToken != "NOT LOGED IN") {
			//korisnikot e najaven
			userToken = JSON.parse(userToken);
			
			var userInfo = JSON.parse(response.data.userInfo);
			var roles = userToken.roles;
			
			for(var i=0; i<roles.length; i++) {
				if(roles[i] == "ROLE_ADMIN") {
					$scope.isAuthenticated = true;
					$scope.isAdmin = true;
				}
				else if(roles[i] == "ROLE_USERS")
					$scope.isAuthenticated = true;
			}
			
			if(!$scope.isAuthenticated) {
				//korisnikot nema pravo da pristapi na /profile stranicata
				$location.path("/");
			}
			
			$scope.userID = userInfo.userID;
			$scope.name = userInfo.name;
			$scope.surname = userInfo.surname;
			$scope.email = userInfo.email;
			$scope.image = userInfo.image;
		}
		else {
			//korisnikot ne e najaven
		}
	});
	
	genreService.genres().get({}, function(response) {
		$scope.genres = response.genres;
	});
	
	moodService.moods().get({}, function(response) {
		$scope.moods = response.moods;
	});
	
	$scope.$watch("videos", function (newValue, oldValue) {
		$timeout(function() {
			$('.gallery').magnificPopup({
				type: 'iframe',
				iframe: {
					markup: '<div class="mfp-iframe-scaler">' +
					'<div class="mfp-close"></div>' +
					'<iframe style="background: #fff none repeat scroll 0 0 !important" class="mfp-iframe" frameborder="0" allowfullscreen></iframe>' +
					'</div>', 
					patterns: {
						youtube: {
						      index: 'youtube.com/', // String that detects type of video (in this case YouTube). Simply via url.indexOf(index).

						      id: 'v=', // String that splits URL in a two parts, second part should be %id%
						      // Or null - full URL will be returned
						      // Or a function that should return %id%, for example:
						      // id: function(url) { return 'parsed id'; } 

						      src: '//www.youtube.com/embed/%id%?autoplay=1' // URL that will be set as a source for iframe. 
						    },
	                }
				},
				delegate: '.videos',
					gallery: {
					enabled: true
				}
			});
		});
	});
	
	$scope.slickConfig = {
		autoplay: false,
		draggable: true,
		dots: false,
		method: {},
		event: {
			beforeChange: function (event, slick, currentSlide, nextSlide) {
			},
			afterChange: function (event, slick, currentSlide, nextSlide) {
			}
		}
	};
	
	$scope.setGenreValue = function(genre) {
		if($scope.listOfSelectedGenres.length < 5) {
			$scope.isGenreDropdownDisabled = false;
			$scope.selectedGenre = genre;
		
			if($scope.listOfSelectedGenres.indexOf(genre) == -1)
				$scope.listOfSelectedGenres.push(genre);
			else
			{
				swal({
					title: "Error",
				    text: "You have already choose that genre for this playlist!",
				    type: "warning",
				    showCancelButton: false,
				    confirmButtonClass: 'btn-warning',
				    confirmButtonText: 'Close'
				});
			}
		}
		else {
			swal({
				title: "Error",
			    text: "You can not enter more than five genres!",
			    type: "warning",
			    showCancelButton: false,
			    confirmButtonClass: 'btn-warning',
			    confirmButtonText: 'Close'
			});
			$scope.isGenreDropdownDisabled = true;
			$scope.selectedGenre = null;
		}
	}
	
	$scope.setMoodValue = function(mood) {
		if($scope.listOfSelectedMoods.length < 5) {
			$scope.isMoodDropdownDisabled = false;
			$scope.selectedMood = mood;
		
			if($scope.listOfSelectedMoods.indexOf(mood) == -1)
				$scope.listOfSelectedMoods.push(mood);
			else
			{
				swal({
					title: "Error",
				    text: "You have already choose that mood for this playlist!",
				    type: "warning",
				    showCancelButton: false,
				    confirmButtonClass: 'btn-warning',
				    confirmButtonText: 'Close'
				}); 
			}
		}
		else {
			swal({
				title: "Error",
			    text: "You can not enter more than five moods!",
			    type: "warning",
			    showCancelButton: false,
			    confirmButtonClass: 'btn-warning',
			    confirmButtonText: 'Close'
			});
			$scope.isMoodDropdownDisabled = true;
			$scope.selectedMood = null;
		}
	}
	
	$scope.deleteGenre = function(genre) {
		$scope.listOfSelectedGenres.splice($scope.listOfSelectedGenres.indexOf(genre), 1);
		$scope.isGenreDropdownDisabled = false;
		
		if($scope.listOfSelectedGenres.length == 0)
			$scope.selectedGenre = null;
	}
	
	$scope.deleteMood = function(mood) {
		$scope.listOfSelectedMoods.splice($scope.listOfSelectedMoods.indexOf(mood), 1);
		$scope.isMoodDropdownDisabled = false;
		
		if($scope.listOfSelectedMoods.length == 0)
			$scope.selectedMood = null;
	}
	
	$scope.generatePlaylist = function() {
		if($scope.listOfSelectedMoods.length == 0 || $scope.listOfSelectedGenres.length == 0)
		{
			swal({
				title: "Error",
			    text: "Please choose mood or genre to generate playlist!",
			    type: "warning",
			    showCancelButton: false,
			    confirmButtonClass: 'btn-warning',
			    confirmButtonText: 'Close'
			});
		}
		else {
			var moods = "";
			var genres = "";
			
			moods = $scope.listOfSelectedMoods[0].name;
			genres = $scope.listOfSelectedGenres[0].name;
			
			for(var i=1; i<$scope.listOfSelectedMoods.length; i++)
				if($scope.listOfSelectedMoods[i] != null)
					moods += "," + $scope.listOfSelectedMoods[i].name;

			for(var i=1; i<$scope.listOfSelectedGenres.length; i++)
				if($scope.listOfSelectedGenres[i] != null)
					genres += "," + $scope.listOfSelectedGenres[i].name;
			
			videoService.youTubeArtistVideos().get({genres : genres, moods : moods}, function(response) {
				$scope.videos = JSON.parse(response.videos);
				console.log($scope.videos);
				$scope.showBtn = false;
				//insert into every video embed to play it
				for(var i=0; i<$scope.videos.length; i++)
					$scope.videos[i].embed = "http://www.youtube.com/watch?v=" + $scope.videos[i].id.videoId;
			});
		}
	}
	
	$scope.savePlaylist = function() {
		//creating new json object
		var playlist = {};
		$scope.image2= {};
		
		$scope.image2.path = "http://www.acpny.com/App_Themes/ACP/images/Loading.gif";
		
		playlist.userID = $scope.userID;
		playlist.name = $scope.playlistObj.name;
		playlist.genres = $scope.listOfSelectedGenres;
		playlist.moods = $scope.listOfSelectedMoods;
		playlist.videos = $scope.videos;
		
		playlistService.generatePlaylist().save(playlist, function(response) {
			if(response.error) {
				swal({
					title: "Error",
				    text: response.error,
				    type: "error",
				    showCancelButton: false,
				    confirmButtonClass: 'btn-danger',
				    confirmButtonText: 'Close'
				});
			}
			else {
				swal({
					title: "Success",
				    text: "You successfully create playlist " + $scope.playlistObj.name,
				    type: "success",
				    showCancelButton: false,
				    showConfirmButton: false
				});
				
				$timeout(function() {
				}, 2000);
				
				var playlistID = response.id;
				$location.path('/playlists/' + playlistID);
			}
		}, function(error) {
			alert(error);
		});
	}
}]);
