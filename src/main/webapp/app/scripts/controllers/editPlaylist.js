'use strict';

//EditPlaylist Controller
PlaylistgenApp.controller('EditPlaylistCtrl', ['$scope', 'playlistService', 'genreService', 'moodService', 'videoService', '$sce', '$window', function($scope, playlistService, genreService, moodService, videoService, $sce, $window) {
	$scope.btnText = "Save";
	$scope.btnClass = [];
	console.log($scope.editPlaylist)
	var obj = {
		name : $scope.editPlaylist.name
	};
	$scope.playlistName = obj.name;
	
	//Genres
	$scope.isGenreDropdownDisabled = false;
	$scope.selectedGenre = null;
	$scope.newGenre = null;
	$scope.playlistGenres = $scope.editPlaylist.genres;
	
	//Moods
	$scope.isMoodDropdownDisabled = false;
	$scope.selectedMood = null;
	$scope.newMood = null;
	$scope.playlistMoods = $scope.editPlaylist.moods;
	
	//Videos
	$scope.videos = [];
	$scope.selectedVideo = null;
	for(var i=0; i<$scope.editPlaylist.videos.length; i++)
		$scope.editPlaylist.videos[i].embed =  $sce.trustAsResourceUrl("http://www.youtube.com/embed/" + $scope.editPlaylist.videos[i].url);
	
	$scope.playlistVideos = $scope.editPlaylist.videos;
	
	$scope.slickConfig = {
			autoplay: false,
			draggable: true,
			dots: false,
			method: {},
			event: {
				beforeChange: function (event, slick, currentSlide, nextSlide) {
				},
			    afterChange: function (event, slick, currentSlide, nextSlide) {
			    }
			}
		};
	
	genreService.genres().get({}, function(response) {
		$scope.genres = response.genres;
	});
	
	moodService.moods().get({}, function(response) {
		$scope.moods = response.moods;
	});
	
	$scope.setGenreValue = function(genre) {
		$scope.selectedGenre = genre;
	}
	
	$scope.setMoodValue = function(mood) {
		$scope.selectedMood = mood;
	}
	
	$scope.checkGenre = function() {
		if($scope.newGenre != '' && $scope.newGenre != null) {
			$scope.isGenreDropdownDisabled = true;
			$scope.selectedGenre = null;
		}
		else
			$scope.isGenreDropdownDisabled = false;
	}
	
	$scope.checkMood = function() {
		if($scope.newMood != '' && $scope.newMood != null) {
			$scope.isMoodDropdownDisabled = true;
			$scope.selectedMood = null;
		}
		else
			$scope.isMoodDropdownDisabled = false;
	}
	
	$scope.addGenre = function() {
		if($scope.selectedGenre == null && ($scope.newGenre == null || $scope.newGenre == ''))
			alert('Please choose one of the following genres or enter new!');
		else if($scope.selectedGenre != null && $scope.newGenre != null)
			alert('You can not select genre from the list and enter new at the same time!');
		else {
			var genre = {};
			
			if($scope.selectedGenre != null)
				genre = $scope.selectedGenre;
			else {
				//creating new json object and put in array
				genre.name = $scope.newGenre;
				$scope.genres.push(genre);
			}
			
			if($scope.playlistGenres.indexOf(genre) == -1) {
				$scope.playlistGenres.push(genre);
				
				$scope.newGenre = null;
				$scope.selectedGenre = null;
				$scope.isGenreDropdownDisabled = false;
			}
			else
				alert('You have already choose that genre for this playlist!');
		}
	}
	
	$scope.addMood = function() {
		if($scope.selectedMood == null && ($scope.newMood == null || $scope.newMood == ''))
			alert('Please choose one of the following moods or enter new!');
		else if($scope.selectedGenre != null && $scope.newMood != null)
			alert('You can not select mood from the list and enter new at the same time!');
		else {
			var mood = {};
			
			if($scope.selectedMood != null)
				mood = $scope.selectedMood;
			else {
				//creating new json object and put in array
				mood.name = $scope.newMood;
				$scope.moods.push(mood);
			}
			
			if($scope.playlistMoods.indexOf(mood) == -1) {
				$scope.playlistMoods.push(mood);
				
				$scope.newMood = null;
				$scope.selectedMood = null;
				$scope.isMoodDropdownDisabled = false;
			}
			else
				alert('You have already choose that mood for this playlist!');
		}
	}
	
	$scope.deleteGenre = function(genre) {
		$scope.playlistGenres.splice($scope.playlistGenres.indexOf(genre), 1);
	}
	
	$scope.deleteMood = function(mood) {
		$scope.playlistMoods.splice($scope.playlistMoods.indexOf(mood), 1);
	}
	
	$scope.searchVideo = function() {
		if($scope.videoTitle == '' ||  $scope.videoTitle == null)
			alert('Please enter the video title or the artist name you want to add!');
		else {
			//reset the YouTube videos
			$scope.videos = [];
			
			videoService.youTubeVideos().get({q : $scope.videoTitle}, function(response) {
				console.log(response);
				
				$scope.videos = response.items;
				for(var i=0; i<$scope.videos.length; i++)
					$scope.videos[i].embed = $sce.trustAsResourceUrl("http://www.youtube.com/embed/" + $scope.videos[i].id.videoId);
			});
		}
	}
	
	$scope.setSelectedVideo = function(video) {
		$scope.selectedVideo = video;
	}
	
	$scope.addVideo = function() {
		if($scope.selectedVideo == null) {
			alert('Please choose one of the following videos!');
		} 
		else {
			//creating new json object and put in array
			var video = {};
			video.image = $scope.selectedVideo.snippet.thumbnails.default.url;
			video.name = $scope.selectedVideo.snippet.title;
			video.description = $scope.selectedVideo.snippet.description;
			video.url = $scope.selectedVideo.id.videoId;
			video.embed = $scope.selectedVideo.embed;
			
			if($scope.playlistVideos.indexOf(video) == -1) {
				$scope.playlistVideos.push(video);
				
			}
			else
				alert('You have already choose that video for this playlist!');
		}
		
		console.log($scope.playlistVideos);
	}
	
	$scope.deleteVideo = function(video) {
		$scope.playlistVideos.splice($scope.playlistVideos.indexOf(video), 1);
	}
	
	$scope.editPlaylistt = function() {
		var playlist = {};
		
		if($scope.playlistName == $scope.editPlaylist.name)
			playlist.newName = false;
		else
			playlist.newName = true;
		
		playlist.name = $scope.playlistName;
		playlist.genres = $scope.playlistGenres;
		playlist.moods = $scope.playlistMoods;
		playlist.videos = $scope.playlistVideos;
		
		if(playlist.name != "" && playlist.genres.length > 0 && playlist.moods.length > 0 && playlist.videos.length > 0) {
			$scope.btnText = "Saving...";
			$scope.btnClass.push('glyphicon glyphicon-refresh glyphicon-refresh-animate');
			
			
			playlistService.playlists().update({id : $scope.editPlaylist.playlistID}, playlist, function(response) {
				if(response.message == "success") {
					$window.location.reload();
				}
				else
					alert(response.message);
			});
		}
		else
			alert("You must fill all the fields to edit " + playlist.name + " playlist!");
	}
}]);
	