'use strict';

//CreatePlaylist Controller
PlaylistgenApp.controller('CreatePlaylistCtrl', ['$scope', '$window', 'playlistService', 'videoService', 'genreService', 'moodService', '$sce', function($scope, $window, playlistService, videoService, genreService, moodService, $sce) {
	$scope.btnText = "Create";
	$scope.btnClass = [];
	
	//Genres
	$scope.isGenreDropdownDisabled = false;
	$scope.selectedGenre = null;
	$scope.newGenre = null;
	$scope.listOfSelectedGenres = [];
	
	//Moods
	$scope.isMoodDropdownDisabled = false;
	$scope.selectedMood = null;
	$scope.newMood = null;
	$scope.listOfSelectedMoods = [];
	
	//Videos
	$scope.videos = [];
	$scope.selectedVideo = null;
	$scope.listOfSelectedVideos = [];
	
	genreService.genres().get({}, function(response) {
		$scope.genres = response.genres;
	});
	
	moodService.moods().get({}, function(response) {
		$scope.moods = response.moods;
	});
	
	$scope.slickConfig = {
		autoplay: false,
		draggable: true,
		dots: false,
		method: {},
		event: {
			beforeChange: function (event, slick, currentSlide, nextSlide) {
			},
		    afterChange: function (event, slick, currentSlide, nextSlide) {
		    }
		}
	};
	
	$scope.setGenreValue = function(genre) {
		$scope.selectedGenre = genre;
	}
	
	$scope.setMoodValue = function(mood) {
		$scope.selectedMood = mood;
	}
	
	$scope.checkGenre = function() {
		if($scope.newGenre != '' && $scope.newGenre != null) {
			$scope.isGenreDropdownDisabled = true;
			$scope.selectedGenre = null;
		}
		else
			$scope.isGenreDropdownDisabled = false;
	}
	
	$scope.checkMood = function() {
		if($scope.newMood != '' && $scope.newMood != null) {
			$scope.isMoodDropdownDisabled = true;
			$scope.selectedMood = null;
		}
		else
			$scope.isMoodDropdownDisabled = false;
	}
	
	$scope.addGenre = function() {
		if($scope.selectedGenre == null && ($scope.newGenre == null || $scope.newGenre == ''))
			alert('Please choose one of the following genres or enter new!');
		else if($scope.selectedGenre != null && $scope.newGenre != null)
			alert('You can not select genre from the list and enter new at the same time!');
		else {
			var genre = {};
			
			if($scope.selectedGenre != null)
				genre = $scope.selectedGenre;
			else {
				//creating new json object and put in array
				genre.name = $scope.newGenre;
				$scope.genres.push(genre);
			}
			
			if($scope.listOfSelectedGenres.indexOf(genre) == -1) {
				$scope.listOfSelectedGenres.push(genre);
				
				$scope.newGenre = null;
				$scope.selectedGenre = null;
				$scope.isGenreDropdownDisabled = false;
			}
			else
				alert('You have already chose that genre for this playlist!');
		}
	}
	
	$scope.addMood = function() {
		if($scope.selectedMood == null && ($scope.newMood == null || $scope.newMood == ''))
			alert('Please choose one of the following moods or enter new!');
		else if($scope.selectedGenre != null && $scope.newMood != null)
			alert('You can not select mood from the list and enter new at the same time!');
		else {
			var mood = {};
			
			if($scope.selectedMood != null)
				mood = $scope.selectedMood;
			else {
				//creating new json object and put in array
				mood.name = $scope.newMood;
				$scope.moods.push(mood);
			}
			
			if($scope.listOfSelectedMoods.indexOf(mood) == -1) {
				$scope.listOfSelectedMoods.push(mood);
				
				$scope.newMood = null;
				$scope.selectedMood = null;
				$scope.isMoodDropdownDisabled = false;
			}
			else
				alert('You have already chose that mood for this playlist!');
		}
	}
	
	$scope.deleteGenre = function(genre) {
		$scope.listOfSelectedGenres.splice($scope.listOfSelectedGenres.indexOf(genre), 1);
	}
	
	$scope.deleteMood = function(mood) {
		$scope.listOfSelectedMoods.splice($scope.listOfSelectedMoods.indexOf(mood), 1);
	}
	
	$scope.searchVideo = function() {
		if($scope.videoTitle == '' ||  $scope.videoTitle == null)
			alert('Please enter the video title or the artist name you want to add!');
		else {
			//reset the videos
			$scope.videos = [];
			
			videoService.youTubeVideos().get({q : $scope.videoTitle}, function(response) {
				console.log(response);
				
				$scope.videos = response.items;
				for(var i=0; i<$scope.videos.length; i++)
					$scope.videos[i].embed = $sce.trustAsResourceUrl("http://www.youtube.com/embed/" + $scope.videos[i].id.videoId);
			});
		}
	}
	
	$scope.setSelectedVideo = function(video) {
		$scope.selectedVideo = video;
	}
	
	$scope.addVideo = function() {
		if($scope.selectedVideo == null) {
			alert('Please choose one of the following videos!');
		} 
		else {
			if($scope.listOfSelectedVideos.indexOf($scope.selectedVideo) == -1) {
				$scope.listOfSelectedVideos.push($scope.selectedVideo);
				
			}
			else
				alert('You have already choose that video for this playlist!');
		}
		
		console.log($scope.listOfSelectedVideos);
	}
	
	$scope.deleteVideo = function(video) {
		$scope.listOfSelectedVideos.splice($scope.listOfSelectedVideos.indexOf(video), 1);
	}

	$scope.createPlaylist = function() {		
		//creating new json object
		var playlist = {};
		
		playlist.userID = $scope.userID;
		playlist.name = $scope.playlistName;
		playlist.genres = $scope.listOfSelectedGenres;
		playlist.moods = $scope.listOfSelectedMoods;
		playlist.videos = $scope.listOfSelectedVideos;
		
		if(playlist.name != "" && playlist.genres.length > 0 && playlist.moods.length > 0 && playlist.videos.length > 0) {
			$scope.btnText = "Creating...";
			$scope.btnClass.push('glyphicon glyphicon-refresh glyphicon-refresh-animate');
			
			playlistService.playlists().save(playlist, function(response) {
				if(response.error)
					{
						swal({
							title: "Error",
						    text: response.error,
						    type: "error",
						    showCancelButton: false,
						    confirmButtonClass: 'btn-danger',
						    confirmButtonText: 'Close'
						});
					}
				else 
					$window.location.reload();
			}, function(error) {
				swal({
					title: "Error",
				    text: error,
				    type: "error",
				    showCancelButton: false,
				    confirmButtonClass: 'btn-danger',
				    confirmButtonText: 'Close'
				});
			});
		}
		else
			{
				swal({
					title: "Error",
				    text: "Fill all the fields to create new playlist!",
				    type: "warning",
				    showCancelButton: false,
				    confirmButtonClass: 'btn-warning',
				    confirmButtonText: 'Close'
				});
			}
	}
}]);
