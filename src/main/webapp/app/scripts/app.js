'use strict';

/**
 * @ngdoc Definition of the application module. The first argument is the name
 *        of the module. It is used in the ng-app directive to expose the
 *        angular components that can be used. The second argument is an array
 *        that defines the dependencies (modules) that are used by the
 *        application. In this case we are only use the ngRoute module as a
 *        dependency in order to provide partial content inclusion through the
 *        routes
 * @see router.js for more information
 * @name playlistgenApp - the name of the module used in the ng-app
 *       directive
 * @description # playlistgenApp Main module of the application.
 */

var PlaylistgenApp = angular.module('playlistgenApp', ['ui.router', 'ui.bootstrap', 'ngResource', 'ngDialog', 'ng-dropdown', 'angular-loading-bar','nsPopover', 'slickCarousel']);

PlaylistgenApp.run(['$rootScope', '$state', '$stateParams', function ($rootScope, $state, $stateParams) {
	  $rootScope.$state = $state;
	  $rootScope.$stateParams = $stateParams;
	  $state.transitionTo('root.main');
}]);