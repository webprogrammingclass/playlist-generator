<!DOCTYPE html>

<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Playlist Generator</title>

    <!-- Bootstrap core CSS -->
    <link href="${pageContext.request.contextPath}/bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
	
    <!-- Custom styles for this template -->
    <link href="${pageContext.request.contextPath}/bower_components/AngularJS-Toaster/toaster.css" rel="stylesheet">
    <link href="${pageContext.request.contextPath}/bower_components/ngDialog/css/ngDialog.css" rel="stylesheet">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bower_components/ngDialog/css/ngDialog-theme-default.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bower_components/ngDialog/css/ngDialog-theme-plain.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bower_components/ngDialog/css/ngDialog-custom-width.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bower_components/ng-dropdown/dist/css/ng-dropdown.min.css">
    <link rel='stylesheet' href='${pageContext.request.contextPath}/bower_components/angular-loading-bar/build/loading-bar.min.css' type='text/css' media='all' />
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bower_components/slick-carousel/slick/slick.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bower_components/slick-carousel/slick/slick-theme.css">
    <link rel="stylesheet" href="${pageContext.request.contextPath}/bower_components/magnific-popup/dist/magnific-popup.css"> 
    
    <link href="${pageContext.request.contextPath}/styles/mine.css" rel="stylesheet"> 
    
    <link href="${pageContext.request.contextPath}/styles/main.css" rel="stylesheet">
	<link href="${pageContext.request.contextPath}/styles/admin.css" rel="stylesheet">
	
	<link rel="stylesheet" href="http://lipis.github.io/bootstrap-sweetalert/lib/sweet-alert.css">
    <link rel="stylesheet" href="http://ricostacruz.com/nprogress/nprogress.css">
    
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
      <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body ng-app="playlistgenApp">
	<!--[if lt IE 7]>
	<p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade
	    your browser</a> to improve your experience.</p>
	<![endif]-->
	
		<div ui-view="header"></div>
		<br />
		<br />
		<br />
	    <div ui-view="container"></div>
	    <br />
		<br />
	    <div ui-view="footer"></div>
	    
	<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
	
    <!-- Bootstrap core JavaScript -->
  	<script src="${pageContext.request.contextPath}/bower_components/jquery/dist/jquery.js"></script>
	<script src="${pageContext.request.contextPath}/bower_components/angular/angular.js"></script>
	<script src="${pageContext.request.contextPath}/bower_components/bootstrap/dist/js/bootstrap.js"></script>
	<script src="${pageContext.request.contextPath}/bower_components/angular-bootstrap/ui-bootstrap-tpls.js"></script>
	<script src="${pageContext.request.contextPath}/bower_components/angular-animate/angular-animate.js"></script>
	<script src="${pageContext.request.contextPath}/bower_components/AngularJS-Toaster/toaster.js"></script>
	<script src="${pageContext.request.contextPath}/bower_components/angular-resource/angular-resource.js"></script>
	<script src="${pageContext.request.contextPath}/bower_components/ngDialog/js/ngDialog.js"></script>
	<script src="${pageContext.request.contextPath}/bower_components/angular-ui-router/js/angular-ui-router.js"></script>
	<script src="${pageContext.request.contextPath}/bower_components/flow.js/dist/flow.min.js"></script>
	<script src="${pageContext.request.contextPath}/bower_components/ng-flow/dist/ng-flow.min.js"></script>
	<script src="${pageContext.request.contextPath}/bower_components/ng-dropdown/dist/js/ng-dropdown.min.js"></script>
	<script src="${pageContext.request.contextPath}/bower_components/angular-loading-bar/build/loading-bar.min.js"></script>
	<script src="${pageContext.request.contextPath}/bower_components/slick-carousel/slick/slick.js"></script>
    <script src="${pageContext.request.contextPath}/bower_components/angular-slick-carousel/dist/angular-slick.min.js"></script>
    <script src="${pageContext.request.contextPath}/bower_components/nsPopover/src/nsPopover.js"></script>
    <script src="${pageContext.request.contextPath}/bower_components/magnific-popup/dist/jquery.magnific-popup.js"></script>
	<script src="http://lipis.github.io/bootstrap-sweetalert/lib/sweet-alert.js"></script>
	<!-- endbower -->

	<!-- These scripts hold the code of the application -->
	
	<!-- The definition and the configuration of the application module -->
	<script src="${pageContext.request.contextPath}/scripts/app.js"></script>
	<!-- The route configuration -->
	<script src="${pageContext.request.contextPath}/scripts/router.js"></script>
	<!-- controllers definition -->
	<script src="${pageContext.request.contextPath}/scripts/controllers/header.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/controllers/main.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/controllers/login.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/controllers/admin.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/controllers/createPlaylist.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/controllers/deletePlaylist.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/controllers/editPlaylist.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/controllers/changePassword.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/controllers/profile.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/controllers/generatePlaylist.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/controllers/search.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/controllers/playlists.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/controllers/playlist.js"></script>
	<!-- Services definition -->
	<script src="${pageContext.request.contextPath}/scripts/services/playlist.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/services/video.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/services/user.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/services/mood.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/services/genre.js"></script>
	<!-- Directives definition -->
	<script src="${pageContext.request.contextPath}/scripts/directives/ngEnter.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/directives/ngFocus.js"></script>
	<script src="${pageContext.request.contextPath}/scripts/directives/starRating.js"></script>
	<!-- endbuild -->
  </body>
</html>
