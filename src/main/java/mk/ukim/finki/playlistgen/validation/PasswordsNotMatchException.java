package mk.ukim.finki.playlistgen.validation;

@SuppressWarnings("serial")
public class PasswordsNotMatchException extends Throwable {
	public PasswordsNotMatchException(String message) {
		super(message);
	}
}
