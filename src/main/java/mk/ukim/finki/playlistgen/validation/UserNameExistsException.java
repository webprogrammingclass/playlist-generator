package mk.ukim.finki.playlistgen.validation;

@SuppressWarnings("serial")
public class UserNameExistsException extends Throwable {
	public UserNameExistsException(String message) {
		super(message);
	}
}
