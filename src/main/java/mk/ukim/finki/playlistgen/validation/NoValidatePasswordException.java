package mk.ukim.finki.playlistgen.validation;

@SuppressWarnings("serial")
public class NoValidatePasswordException extends Throwable {

	public NoValidatePasswordException(String message) {
		super(message);
	}
}
