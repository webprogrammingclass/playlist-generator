package mk.ukim.finki.playlistgen.validation;

@SuppressWarnings("serial")
public class PasswordDoesntExistsException extends Throwable {
	public PasswordDoesntExistsException(String message) {
		super(message);
	}
}
