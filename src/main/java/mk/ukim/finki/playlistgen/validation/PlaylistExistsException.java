package mk.ukim.finki.playlistgen.validation;

@SuppressWarnings("serial")
public class PlaylistExistsException extends Throwable {

	public PlaylistExistsException(String message) {
		super(message);
	}
}
