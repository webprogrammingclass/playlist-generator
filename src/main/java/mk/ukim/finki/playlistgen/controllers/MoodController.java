package mk.ukim.finki.playlistgen.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mk.ukim.finki.playlistgen.model.Mood;
import mk.ukim.finki.playlistgen.services.MoodService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/moods")
public class MoodController {
	@Autowired
	private MoodService moodService;
	
	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String saveAllMoods(@RequestBody String moods, HttpServletRequest request, HttpServletResponse response) throws JSONException {
		JSONArray jsonArray = new JSONArray(moods);
		List<Mood> tmp_moods = new ArrayList<Mood>();
		
		for(int i=0; i<jsonArray.length(); i++) {
			Mood m = new Mood(jsonArray.getJSONObject(i).getString("name"));
			tmp_moods.add(m);
		}
		
		JSONObject jsonobj = new JSONObject();
		
		if(moodService.saveAllMoods(tmp_moods))
			jsonobj.put("data", "OK");
		else 
			jsonobj.put("data", "ERROR");
		
		return jsonobj.toString();
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String getAllMoods(HttpServletRequest request, HttpServletResponse response) {
		JSONObject json = new JSONObject();
		JSONArray array = new JSONArray();
		
		List<Mood> moods = moodService.getAllMoods();
		
		for(int i=0; i<moods.size(); i++) {
			JSONObject obj = new JSONObject();
			
			obj.put("name", moods.get(i).getMoodName());
			array.put(obj);
		}
		
		json.put("moods", array);
		return json.toString();
	}
}
