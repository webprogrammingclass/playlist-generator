package mk.ukim.finki.playlistgen.controllers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mk.ukim.finki.playlistgen.comparator.ChooseGenreComparator;
import mk.ukim.finki.playlistgen.comparator.ChooseMoodComparator;
import mk.ukim.finki.playlistgen.model.ChooseGenre;
import mk.ukim.finki.playlistgen.model.ChooseMood;
import mk.ukim.finki.playlistgen.model.FavoritePlaylist;
import mk.ukim.finki.playlistgen.model.Genre;
import mk.ukim.finki.playlistgen.model.HasGenre;
import mk.ukim.finki.playlistgen.model.HasMood;
import mk.ukim.finki.playlistgen.model.HasPlaylist;
import mk.ukim.finki.playlistgen.model.HasVideo;
import mk.ukim.finki.playlistgen.model.Mood;
import mk.ukim.finki.playlistgen.model.Playlist;
import mk.ukim.finki.playlistgen.model.RatedPlaylist;
import mk.ukim.finki.playlistgen.model.User;
import mk.ukim.finki.playlistgen.model.Video;
import mk.ukim.finki.playlistgen.security.TokenTransfer;
import mk.ukim.finki.playlistgen.security.TokenUtils;
import mk.ukim.finki.playlistgen.services.UserService;
import mk.ukim.finki.playlistgen.validation.EmailExistsException;
import mk.ukim.finki.playlistgen.validation.NoValidatePasswordException;
import mk.ukim.finki.playlistgen.validation.PasswordDoesntExistsException;
import mk.ukim.finki.playlistgen.validation.PasswordsNotMatchException;
import mk.ukim.finki.playlistgen.validation.UserNameExistsException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.logout.CookieClearingLogoutHandler;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/users")
public class UserController {
	private static final int TOKEN_DURATION = 30 * 24 * 60 * 60; // 30 days

	@Autowired
	private UserDetailsService userDetailsService;
	
	@Autowired
    private UserService userService;
	
	@Autowired
	@Qualifier("authenticationManager")
	private AuthenticationManager authManager;
	
	@RequestMapping(value = "/authenticate", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public TokenTransfer authenticate( @RequestParam("username") String username, @RequestParam("password") String password, @RequestParam("rememberMe") boolean rememberMe, HttpServletRequest request, HttpServletResponse response) throws JSONException, IOException, NoValidatePasswordException {
		UserDetails userDetails = this.userDetailsService.loadUserByUsername(username);
		
		try {
			if(userService.checkUser(password, username)) {
				Cookie cookie = createCookie(userDetails);
				
				System.out.println("Created cookie with username: " + userDetails.getUsername() + " and role: " + userDetails.getAuthorities());
				
				if (rememberMe) {
					cookie.setMaxAge(TOKEN_DURATION);
				}
				cookie.setPath(request.getContextPath());
				response.addCookie(cookie);
				
				return new TokenTransfer(TokenUtils.createToken(userDetails).toString());
			}
			
			return null;
		}
		catch(NoValidatePasswordException e) {
			return new TokenTransfer(e.getMessage());
		}
	}
	
	@RequestMapping(value = "/register", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public TokenTransfer createUser( @RequestParam("name") String name, @RequestParam("lastname") String lastname, @RequestParam("username") String username, @RequestParam("password") String password, @RequestParam("repassword") String repassword, @RequestParam("email") String email,  @RequestParam("age") String age, @RequestParam("sex") String sex, HttpServletRequest request, HttpServletResponse response) throws JSONException {
		int hummanity = (sex == "male") ? 0 : 1;
		int ageNumber = Integer.parseInt(age);
		
		User user = null;
		UserDetails userDetails = null;
		
		try {
			user = createUserAccount(username, email, password, ageNumber, name, lastname, hummanity);
			
			if(user != null) {
				userDetails = this.userDetailsService.loadUserByUsername(username);
				
				Cookie cookie = createCookie(userDetails);
				
				System.out.println("Created cookie with username: " + userDetails.getUsername() + " and role: " + userDetails.getAuthorities());
				
				cookie.setPath(request.getContextPath());
				response.addCookie(cookie);
			}
		} catch (EmailExistsException e) {
			return new TokenTransfer(e.getMessage());
		} catch (UserNameExistsException e) {
			return new TokenTransfer(e.getMessage());
		}
		
		return new TokenTransfer(TokenUtils.createToken(userDetails).toString());
	}
	
	@RequestMapping(value = "/get_user_token", method = RequestMethod.GET,  produces = "application/json")
	@ResponseBody
	public String getToken(HttpServletRequest request, HttpServletResponse response) throws JSONException {
		Cookie[] cookies = request.getCookies();
		
		String token = userService.getUserToken(cookies, "token");
		
		if(token != null)
			return token;
		
		JSONObject json = new JSONObject();
		json.put("token", "NOT LOGED IN");
		
		return json.toString();
	}
	
	@RequestMapping(value = "/delete_user_token", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String deleteToken(HttpServletRequest request, HttpServletResponse response) throws JSONException, ServletException {
		JSONObject json = new JSONObject();
		
		CookieClearingLogoutHandler cookieClearingLogoutHandler = new CookieClearingLogoutHandler("token");
	    SecurityContextLogoutHandler securityContextLogoutHandler = new SecurityContextLogoutHandler();
	    
	    cookieClearingLogoutHandler.logout(request, response, null);
	    securityContextLogoutHandler.logout(request, response, null);

		json.put("token", "DELETED COOKIE");
		return json.toString();
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = "application/json")
	@ResponseBody
	public String editUser(@PathVariable(value="id") String id, @RequestBody String user, HttpServletRequest request, HttpServletResponse response) throws JSONException, ServletException {
		Long userID = Long.parseLong(id);
		
		JSONObject json = new JSONObject(user);
		
		if(json.getBoolean("flag") == true) {
			String oldPass = json.getString("oldPassword");
			String newPass = json.getString("newPassword");
			String newPassConf = json.getString("newPasswordConf");
			
			try {
				userService.changePassword(userID, oldPass, newPass, newPassConf);
				
				this.deleteToken(request, response);
				
				json = new JSONObject();
				json.put("token", "DELETED COOKIE");
				return json.toString();
			} catch (PasswordsNotMatchException e) {
				return new JSONObject("{\"error\": " + e.getMessage() + "}").toString();
			} catch (PasswordDoesntExistsException e) {
				return new JSONObject("{\"error\": " + e.getMessage() + "}").toString();
			}
		}
		else {
			return "dasdasd";
		}
	}
	
	@RequestMapping(value="/{id}/tags", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String getUserMoodsAndGenres(@PathVariable(value="id") String id, HttpServletRequest request, HttpServletResponse response) throws JSONException {
		Long userID = Long.parseLong(id);
		
		JSONObject json = new JSONObject();
		JSONArray moods = new JSONArray();
		JSONArray genres = new JSONArray();
		
		List<ChooseMood> userMoods = userService.getUserMoods(userID);
		List<ChooseGenre> userGenres = userService.getUserGenres(userID);
		
		Collections.sort(userMoods, new ChooseMoodComparator());
		Collections.sort(userGenres, new ChooseGenreComparator());
		
		//not more than 5 preferred moods and genres
		int genreSize = (userGenres.size() > 5) ? 5 : userGenres.size();
		int moodsSize = (userMoods.size() > 5) ? 5 : userMoods.size();
		
		for(int i=0; i<genreSize; i++) {
			JSONObject genre = new JSONObject();
			genre.put("name", userGenres.get(i).getGenre().getGenreName());
			genre.put("count", userGenres.get(i).getCount());
			
			genres.put(genre);
		}
		
		for(int i=0; i<moodsSize; i++) {
			JSONObject mood = new JSONObject();
			mood.put("name", userMoods.get(i).getMood().getMoodName());
			mood.put("count", userMoods.get(i).getCount());
			
			moods.put(mood);
		}
		
		json.put("userGenres", genres);
		json.put("userMoods", moods);
		
		return json.toString();
	}
	
	@RequestMapping(value="/{id}/playlists", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String getUserPlaylists(@PathVariable(value="id") String id, Pageable pageable, HttpServletRequest request, HttpServletResponse response) throws JSONException {
		Long userID = Long.parseLong(id);
		
		JSONObject json = new JSONObject();
		JSONArray userPlaylists = new JSONArray();
		JSONObject pagable = new JSONObject();
		
		Page<HasPlaylist> page = userService.getUserPlaylists(userID, pageable);
		List<HasPlaylist> hasPlaylists = page.getContent();
		
		List<Playlist> playlists = new ArrayList<Playlist>();
		for(int i=0; i<hasPlaylists.size(); i++) 
			playlists.add(hasPlaylists.get(i).getPlaylist());
		
		pagable.put("totalPages", page.getTotalPages());
		pagable.put("totalElements", page.getTotalElements());
		pagable.put("page", page.getNumber());
		
		userPlaylists = createPlaylistsArray(playlists);
		
		json.put("pageable", pagable);
		json.put("userPlaylists", userPlaylists);
		
		return json.toString();
	}
	
	@RequestMapping(value="/{id}/favorites", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String getUserFavoritePlaylists(@PathVariable(value="id") String id, Pageable pageable, HttpServletRequest request, HttpServletResponse response) throws JSONException {
		Long userID = Long.parseLong(id);
		
		JSONObject json = new JSONObject();
		JSONArray favorites = new JSONArray();
		JSONObject pagable = new JSONObject();
		
		Page<FavoritePlaylist> page = userService.getUserFavoritesPlaylists(userID, pageable);
		List<FavoritePlaylist> favoritePlaylists = page.getContent();
		
		List<Playlist> playlists = new ArrayList<Playlist>();
		for(int i=0; i<favoritePlaylists.size(); i++)
			playlists.add(favoritePlaylists.get(i).getPlaylist());
		
		pagable.put("totalPages", page.getTotalPages());
		pagable.put("totalElements", page.getTotalElements());
		pagable.put("page", page.getNumber());
		
		favorites = createPlaylistsArray(playlists);
		
		json.put("pageable", page);
		json.put("userFavoritePlaylists", favorites);
		
		return json.toString();
	}
	
	@RequestMapping(value="/{id}/favorites/{playlistID}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String getUserFavoritePlaylist(@PathVariable(value="id") String id, @PathVariable(value="playlistID") String playlistID, HttpServletRequest request, HttpServletResponse response) throws JSONException {
		Long userID = Long.parseLong(id);
		Long playlist_id = Long.parseLong(playlistID);
		
		JSONObject json = new JSONObject();
		
		FavoritePlaylist favoritePlaylist = userService.getUserFavoritePlaylist(userID, playlist_id);
		
		if(favoritePlaylist == null)
			json.put("message", "NO");
		else
			json.put("message", "YES");
		
		return json.toString();
	}
	
	@RequestMapping(value="/{id}/favorites/{playlistID}", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String setUserFavoritePlaylist(@PathVariable(value="id") String id, @PathVariable(value="playlistID") String playlistID, HttpServletRequest request, HttpServletResponse response) throws JSONException {
		Long userID = Long.parseLong(id);
		Long playlist_id = Long.parseLong(playlistID);
		
		JSONObject json = new JSONObject();
		
		if(userService.setUserFavoritePlaylist(userID, playlist_id))
			json.put("message", "YES");
		else
			json.put("message", "NO");
		
		return json.toString();
	}
	
	@RequestMapping(value="/{id}/favorites/{playlistID}", method = RequestMethod.DELETE, produces = "application/json")
	@ResponseBody
	public String deleteUserFavoritePlaylist(@PathVariable(value="id") String id, @PathVariable(value="playlistID") String playlistID, HttpServletRequest request, HttpServletResponse response) throws JSONException {
		Long userID = Long.parseLong(id);
		Long playlist_id = Long.parseLong(playlistID);
		
		JSONObject json = new JSONObject();
		
		if(userService.deleteUserFavoritePlaylist(userID, playlist_id))
			json.put("message", "YES");
		else
			json.put("message", "NO");
		
		return json.toString();
	}
	
	@RequestMapping(value="/{id}/rated", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String getUserRatedPlaylists(@PathVariable(value="id") String id, Pageable pageable, HttpServletRequest request, HttpServletResponse response) throws JSONException {
		Long userID = Long.parseLong(id);
		
		JSONObject json = new JSONObject();
		JSONArray rated = new JSONArray();
		JSONObject pagable = new JSONObject();
		
		Page<RatedPlaylist> page = userService.getUserRatedPlaylists(userID, pageable);
		List<RatedPlaylist> ratedPlaylists = page.getContent();
		
		List<Playlist> playlists = new ArrayList<Playlist>();
		for(int i=0; i<ratedPlaylists.size(); i++)
			playlists.add(ratedPlaylists.get(i).getPlaylist());
		
		pagable.put("totalPages", page.getTotalPages());
		pagable.put("totalElements", page.getTotalElements());
		pagable.put("page", page.getNumber());
		
		rated = createPlaylistsArray(playlists);
		
		json.put("pageable", page);
		json.put("userRatedPlaylists", rated);
		
		return json.toString();
	}
	
	@RequestMapping(value="/{id}/rated/{playlistID}", method = RequestMethod.GET, produces = "application/json")
	@ResponseBody
	public String getUserRatedPlaylist(@PathVariable(value="id") String id, @PathVariable(value="playlistID") String playlistID, HttpServletRequest request, HttpServletResponse response) throws JSONException {
		Long userID = Long.parseLong(id);
		Long playlist_id = Long.parseLong(playlistID);
		
		JSONObject json = new JSONObject();
		
		RatedPlaylist ratedPlaylist = userService.getUserRatedPlaylist(userID, playlist_id);
		
		if(ratedPlaylist == null)
			json.put("message", "NO");
		else json.put("message", "YES");
		
		return json.toString();
	}
	
	@RequestMapping(value="/{id}/rated/{playlistID}", method = RequestMethod.POST, produces = "application/json")
	@ResponseBody
	public String setUserRatedPlaylist(@PathVariable(value="id") String id, @PathVariable(value="playlistID") String playlistID, @RequestBody String r, HttpServletRequest request, HttpServletResponse response) throws JSONException {
		Long userID = Long.parseLong(id);
		Long playlist_id = Long.parseLong(playlistID);
		JSONObject data = new JSONObject(r);
		float rating = (float) data.getInt("rating");
		JSONObject json = new JSONObject();
		
		if(userService.setUserRatedPlaylist(userID, playlist_id, rating))
			json.put("message", "YES");
		else json.put("message", "NO");
		
		return json.toString();
	}
	
	private JSONArray createPlaylistsArray(List<Playlist> playlists) throws JSONException {
		JSONArray favorites = new JSONArray();
		
		for(int i=0; i<playlists.size(); i++) {
			JSONObject obj = new JSONObject();
			
			obj.put("playlistID", playlists.get(i).getPlaylistId());
			obj.put("name", playlists.get(i).getPlaylistName());
			obj.put("rating", playlists.get(i).getRating());
			obj.put("popularity", playlists.get(i).getPopularity());
			obj.put("numberOfVideos", playlists.get(i).getNumVideos());
			obj.put("date", playlists.get(i).getDateOfCreation());
			
			JSONArray videos = new JSONArray();
			JSONArray genres = new JSONArray();
			JSONArray moods = new JSONArray();
				
			Iterator<HasVideo> iterator = playlists.get(i).getHasVideos().iterator();
			Iterator<HasGenre> iterator2 = playlists.get(i).getHasGenres().iterator();
			Iterator<HasMood> iterator3 = playlists.get(i).getHasMood().iterator();
				
			while(iterator.hasNext()) {
				Video v = iterator.next().getVideo();
					
				JSONObject video = new JSONObject();
				video.put("name", v.getVideoTitle());
				video.put("description", v.getVideoDescription());
				video.put("url", v.getVideoEmbed());
				video.put("image", v.getVideoImage());
				
				videos.put(video);
			}
				
			while(iterator2.hasNext()) {
				Genre g = iterator2.next().getGenre();
				
				JSONObject genre = new JSONObject();
				genre.put("name", g.getGenreName());
				
				genres.put(genre);
			}
				
			while(iterator3.hasNext()) {
				Mood m = iterator3.next().getMood();
				
				JSONObject mood = new JSONObject();
				mood.put("name", m.getMoodName());
				
				moods.put(mood);
			}
			
			obj.put("videos", videos);
			obj.put("genres", genres);
			obj.put("moods", moods);
				
			favorites.put(obj);
		}
		
		return favorites;
	}
	
	private Cookie createCookie(UserDetails userDetails) throws JSONException {
		UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userDetails.getUsername(), userDetails.getPassword());
		
		Authentication authentication = this.authManager.authenticate(authenticationToken);
		
		SecurityContextHolder.getContext().setAuthentication(authentication);
		
		return new Cookie("token", TokenUtils.createToken(userDetails).toString());
	}
	
	private User createUserAccount(String username, String email, String password, int ageNumber, String name, String lastname, int hummanity) throws EmailExistsException, UserNameExistsException {
        return userService.registerNewUserAccount(username, email, password, ageNumber, name, lastname, hummanity);
    }
}