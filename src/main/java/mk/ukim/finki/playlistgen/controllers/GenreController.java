package mk.ukim.finki.playlistgen.controllers;

import java.util.*;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mk.ukim.finki.playlistgen.model.Genre;
import mk.ukim.finki.playlistgen.services.GenreService;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/genres")
public class GenreController {
	@Autowired
	private GenreService genreService;
	
	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String saveAllGenres(@RequestBody String genres, HttpServletRequest request, HttpServletResponse response) throws JSONException {
		JSONArray jsonArray = new JSONArray(genres);
		List<Genre> tmp_genres = new ArrayList<Genre>();
		
		for(int i=0; i<jsonArray.length(); i++) {
			Genre g = new Genre(jsonArray.getJSONObject(i).getString("name"));
			tmp_genres.add(g);
		}
		
		JSONObject jsonobj = new JSONObject();
		
		if(genreService.saveAllGenres(tmp_genres))
			jsonobj.put("data", "OK");
		else 
			jsonobj.put("data", "ERROR");
		
		return jsonobj.toString();
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String getAllGenres(HttpServletRequest request, HttpServletResponse response) {
		JSONObject json = new JSONObject();
		JSONArray array = new JSONArray();
		
		List<Genre> genres = genreService.getAllGenres();
		
		for(int i=0; i<genres.size(); i++) {
			JSONObject obj = new JSONObject();
			
			obj.put("name", genres.get(i).getGenreName());
			array.put(obj);
		}
		
		json.put("genres", array);
		return json.toString();
	}
	
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String getGenre(@PathVariable(value="id") String id, HttpServletRequest request, HttpServletResponse response) {
		JSONObject json = new JSONObject();
		
		Genre genre = genreService.getGenre(id);
		
		json.put("genre", genre.getGenreName());
		return json.toString();
	}
}
