package mk.ukim.finki.playlistgen.controllers;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mk.ukim.finki.playlistgen.model.Genre;
import mk.ukim.finki.playlistgen.model.HasGenre;
import mk.ukim.finki.playlistgen.model.HasMood;
import mk.ukim.finki.playlistgen.model.HasPlaylist;
import mk.ukim.finki.playlistgen.model.HasVideo;
import mk.ukim.finki.playlistgen.model.Mood;
import mk.ukim.finki.playlistgen.model.Playlist;
import mk.ukim.finki.playlistgen.model.User;
import mk.ukim.finki.playlistgen.model.Video;
import mk.ukim.finki.playlistgen.services.PlaylistService;
import mk.ukim.finki.playlistgen.services.VideoService;
import mk.ukim.finki.playlistgen.validation.PlaylistExistsException;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/playlists")
public class PlaylistController {
	@Autowired
	PlaylistService playlistService;
	
	@Autowired
	VideoService videoService;
	
	@RequestMapping(value = "/all", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String getAllPlaylists(Pageable pageable, HttpServletRequest request, HttpServletResponse response) throws JSONException {
		JSONObject json = new JSONObject();
		JSONArray array = new JSONArray();
		JSONObject pagable = new JSONObject();
		
		Page<Playlist> page = playlistService.getAllPlaylists(pageable);
		List<Playlist> playlists = page.getContent();

		pagable.put("totalPages", page.getTotalPages());
		pagable.put("totalElements", page.getTotalElements());
		pagable.put("page", page.getNumber());
		
		array = createJSONArray(playlists);
		
		json.put("pagable", pagable);
		json.put("playlists", array);
			
		return json.toString();
	}
	
	@RequestMapping(value = "/newest", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String getNewestPlaylists(HttpServletRequest request, HttpServletResponse response) throws JSONException {
		JSONObject json = new JSONObject();
		JSONArray array = new JSONArray();
		
		List<Playlist> playlists = playlistService.getNewestPlaylists();
		
		array = createJSONArray(playlists);
		
		json.put("playlists", array);
			
		return json.toString();
	}
	
	@RequestMapping(value = "/top-rated", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String getTopRatedPlaylists(HttpServletRequest request, HttpServletResponse response) throws JSONException {
		JSONObject json = new JSONObject();
		JSONArray array = new JSONArray();
		
		List<Playlist> playlists = playlistService.getNewestPlaylists();
		
		array = createJSONArray(playlists);
		
		json.put("playlists", array);
			
		return json.toString();
	}
	
	@RequestMapping(value = "/all/{param}/{userID}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String getPlaylists(@PathVariable(value="param") String param, @PathVariable(value="userID") String userID, Pageable pageable, HttpServletRequest request, HttpServletResponse response) throws JSONException {
		JSONObject json = new JSONObject();
		JSONArray array = new JSONArray();
		JSONObject pagable = new JSONObject();
		
		List<Playlist> playlists = new ArrayList<Playlist>();
		List<User> users = new ArrayList<User>();
		
		Long user_id = Long.parseLong(userID);
		
		if(param.equals("adminplaylists")) {
			Page<HasPlaylist> page = playlistService.getAdminPlaylists(user_id, pageable);
			List<HasPlaylist> hasAdminPlaylist = page.getContent();
			
			for(int i=0; i<hasAdminPlaylist.size(); i++)
				playlists.add(hasAdminPlaylist.get(i).getPlaylist());
			
			pagable.put("totalPages", page.getTotalPages());
			pagable.put("totalElements", page.getTotalElements());
			pagable.put("page", page.getNumber());
		}
		else {
			Page<HasPlaylist> has_page = playlistService.getUsersPlaylists(user_id, pageable);
			List<HasPlaylist> p = has_page.getContent();
			
			for(int i=0; i<p.size(); i++) {
				playlists.add(p.get(i).getPlaylist());
				users.add(p.get(i).getUser());
			}
			
			pagable.put("totalPages", has_page.getTotalPages());
			pagable.put("totalElements", has_page.getTotalElements());
			pagable.put("page", has_page.getNumber());
		}
		
		array = createJSONArray(playlists);
		
		json.put("pagable", pagable);
		json.put("playlists", array);
			
		return json.toString();
	}
	
	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String createPlaylist(@RequestBody String playlist, HttpServletRequest request, HttpServletResponse response) throws JSONException {
		JSONObject json = new JSONObject(playlist);
		
		String playlistName = json.getString("name");
		Long userID = Long.parseLong(json.getString("userID"));
		
		List<Genre> genres = new ArrayList<Genre>();
		List<Mood> moods = new ArrayList<Mood>();
		List<Video> videos = new ArrayList<Video>();
		
		JSONArray tmpGenres = json.getJSONArray("genres");
		JSONArray tmpMoods = json.getJSONArray("moods");
		JSONArray tmpVideos = json.getJSONArray("videos");
		
		if(tmpGenres != null && tmpMoods != null && tmpVideos != null && playlistName != "") {
			for(int i=0; i<tmpGenres.length(); i++) {
				JSONObject jsonObj = tmpGenres.getJSONObject(i);
				
				Genre g = new Genre(jsonObj.getString("name"));
				genres.add(g);
			}
			
			for(int i=0; i<tmpMoods.length(); i++) {
				JSONObject jsonObj = tmpMoods.getJSONObject(i);
				
				Mood m = new Mood(jsonObj.getString("name"));
				moods.add(m);
			}
			
			for(int i=0; i<tmpVideos.length(); i++) {
				JSONObject jsonObj = tmpVideos.getJSONObject(i);
				
				String videoId = jsonObj.getJSONObject("id").getString("videoId");
				String videoName = jsonObj.getJSONObject("snippet").getString("title");
				String videoDescription = jsonObj.getJSONObject("snippet").getString("description");
				String videoImg = jsonObj.getJSONObject("snippet").getJSONObject("thumbnails").getJSONObject("medium").getString("url");
				
				Video v = new Video(videoId, videoName, videoDescription, videoImg);
				videos.add(v);
			}
		}
		else
			return new JSONObject("{\"error\": \"Fill all the fields to create new playlist!\"}").toString();
		
		try {
			 Playlist p = playlistService.createPlaylist(userID, playlistName, videos, moods, genres);
			
			return new JSONObject("{\"id\": " + p.getPlaylistId() + "}").toString();
		} catch (PlaylistExistsException e) {
			return new JSONObject("{\"error\": " + e.getMessage() + "}").toString();
		}
	}
	
	@RequestMapping(value = "/generate", method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String generatePlaylist(@RequestBody String playlist, HttpServletRequest request, HttpServletResponse response) throws JSONException {
		JSONObject json = new JSONObject(playlist);
		
		String playlistName = json.getString("name");
		Long userID = Long.parseLong(json.getString("userID"));
		
		List<Genre> genres = new ArrayList<Genre>();
		List<Mood> moods = new ArrayList<Mood>();
		List<Video> videos = new ArrayList<Video>();
		
		JSONArray tmpGenres = json.getJSONArray("genres");
		JSONArray tmpMoods = json.getJSONArray("moods");
		JSONArray tmpVideos = json.getJSONArray("videos");
		
		if(tmpGenres != null && tmpMoods != null && tmpVideos != null && playlistName != "") {
			for(int i=0; i<tmpGenres.length(); i++) {
				JSONObject jsonObj = tmpGenres.getJSONObject(i);
				
				Genre g = new Genre(jsonObj.getString("name"));
				genres.add(g);
			}
			
			for(int i=0; i<tmpMoods.length(); i++) {
				JSONObject jsonObj = tmpMoods.getJSONObject(i);
				
				Mood m = new Mood(jsonObj.getString("name"));
				moods.add(m);
			}
			
			for(int i=0; i<tmpVideos.length(); i++) {
				JSONObject jsonObj = tmpVideos.getJSONObject(i);
				
				String videoId = jsonObj.getJSONObject("id").getString("videoId");
				String videoName = jsonObj.getJSONObject("snippet").getString("title");
				String videoDescription = jsonObj.getJSONObject("snippet").getString("description");
				String videoImg = jsonObj.getJSONObject("snippet").getJSONObject("thumbnails").getJSONObject("medium").getString("url");
				
				Video v = new Video(videoId, videoName, videoDescription, videoImg);
				videos.add(v);
			}
		}
		else
			return new JSONObject("{\"error\": \"Fill all the fields to create new playlist!\"}").toString();
		
		try {
			 Playlist p = playlistService.generatePlaylist(userID, playlistName, videos, moods, genres);
			
			return new JSONObject("{\"id\": " + p.getPlaylistId() + "}").toString();
		} catch (PlaylistExistsException e) {
			return new JSONObject("{\"error\": " + e.getMessage() + "}").toString();
		}
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String deletePlaylist(@PathVariable(value="id") String id, HttpServletRequest request, HttpServletResponse response) throws JSONException {
		Long playlistID = Long.parseLong(id);
		
		JSONObject jsonobj = new JSONObject();
		
		if(playlistService.removePlaylist(playlistID))
			jsonobj.put("data", "OK");
		else
			jsonobj.put("data", "ERROR");
		
		return jsonobj.toString();
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String editPlaylist(@PathVariable(value="id") String id, @RequestBody String playlist, HttpServletRequest request, HttpServletResponse response) throws JSONException {
		Long playlistID = Long.parseLong(id);
		
		JSONObject json = new JSONObject(playlist);
		
		String playlistName = json.getString("name");
		boolean isNewName = json.getBoolean("newName");
		
		List<Genre> genres = new ArrayList<Genre>();
		List<Mood> moods = new ArrayList<Mood>();
		List<Video> videos = new ArrayList<Video>();
		
		JSONArray tmpGenres = json.getJSONArray("genres");
		JSONArray tmpMoods = json.getJSONArray("moods");
		JSONArray tmpVideos = json.getJSONArray("videos");
		
		if(playlistName != "" && tmpGenres != null && tmpMoods != null && tmpVideos != null) {
			for(int i=0; i<tmpGenres.length(); i++) {
				JSONObject jsonObj = tmpGenres.getJSONObject(i);
				
				Genre g = new Genre(jsonObj.getString("name"));
				genres.add(g);
			}
			
			for(int i=0; i<tmpMoods.length(); i++) {
				JSONObject jsonObj = tmpMoods.getJSONObject(i);
				
				Mood m = new Mood(jsonObj.getString("name"));
				moods.add(m);
			}
			
			for(int i=0; i<tmpVideos.length(); i++) {
				JSONObject jsonObj = tmpVideos.getJSONObject(i);
				
				String videoId = jsonObj.getString("url");
				String videoName = jsonObj.getString("name");
				String videoDescription = jsonObj.getString("description");
				String videoImg = jsonObj.getString("image");
				
				Video v = new Video(videoId, videoName, videoDescription, videoImg);
				videos.add(v);
			}
		}
		else
			return new JSONObject("{\"message\": \"Fill all the fields to create new playlist!\"}").toString();
		
		try {
			Playlist p = null;
			
			p = playlistService.editPlaylist(playlistID, playlistName, videos, moods, genres, isNewName);
			return new JSONObject("{\"message\": \"success\"}").toString();
		} catch (PlaylistExistsException e) {
			return new JSONObject("{\"message\": " + e.getMessage() + "}").toString();
		}
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String getPlaylist(@PathVariable(value="id") String id, HttpServletRequest request, HttpServletResponse response) throws JSONException {
		Long playlistID = Long.parseLong(id);
		
		Playlist playlist = playlistService.getPlaylist(playlistID);
		JSONObject obj = new JSONObject();
		
		obj.put("playlistID", playlist.getPlaylistId());
		obj.put("name", playlist.getPlaylistName());
		obj.put("rating", playlist.getRating());
		obj.put("popularity", playlist.getPopularity());
		obj.put("numberOfVideos", playlist.getNumVideos());
		obj.put("date", playlist.getDateOfCreation());
		obj.put("count", playlist.getCount());
		
		JSONArray videos = new JSONArray();
		JSONArray genres = new JSONArray();
		JSONArray moods = new JSONArray();
		
		Iterator<HasVideo> iterator = playlist.getHasVideos().iterator();
		Iterator<HasGenre> iterator2 = playlist.getHasGenres().iterator();
		Iterator<HasMood> iterator3 = playlist.getHasMood().iterator();
		
		while(iterator.hasNext()) {
			Video v = iterator.next().getVideo();
			
			JSONObject video = new JSONObject();
			video.put("name", v.getVideoTitle());
			video.put("description", v.getVideoDescription());
			video.put("url", v.getVideoEmbed());
			video.put("image", v.getVideoImage());
			
			videos.put(video);
		}
		
		while(iterator2.hasNext()) {
			Genre g = iterator2.next().getGenre();
			
			JSONObject genre = new JSONObject();
			genre.put("name", g.getGenreName());
			
			genres.put(genre);
		}
		
		while(iterator3.hasNext()) {
			Mood m = iterator3.next().getMood();
			
			JSONObject mood = new JSONObject();
			mood.put("name", m.getMoodName());
			
			moods.put(mood);
		}
		
		obj.put("videos", videos);
		obj.put("genres", genres);
		obj.put("moods", moods);
		
		return obj.toString();
	}
	
	@RequestMapping(value = "/search", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String searchPlaylists(@RequestParam(value="q") String query, Pageable pageable, HttpServletRequest request, HttpServletResponse response) throws JSONException {
		JSONObject json = new JSONObject();
		JSONArray array = new JSONArray();
		
		Page<Playlist> page = playlistService.getAdminPlaylistsFromSearch(query, pageable);
		List<Playlist> playlists = page.getContent();
		
		JSONObject pagable = new JSONObject();
		pagable.put("totalPages", page.getTotalPages());
		pagable.put("totalElements", page.getTotalElements());
		pagable.put("page", page.getNumber());
		
		array = createJSONArray(playlists);
		
		json.put("playlists", array);
		json.put("pagable", pagable);
		
		return json.toString();
	}
	
	private JSONArray createJSONArray(List<Playlist> playlists) throws JSONException {
		JSONArray array = new JSONArray();
		
		for(int i=0; i<playlists.size(); i++) {
			JSONObject obj = new JSONObject();
				
			obj.put("playlistID", playlists.get(i).getPlaylistId());
			obj.put("name", playlists.get(i).getPlaylistName());
			obj.put("rating", playlists.get(i).getRating());
			obj.put("popularity", playlists.get(i).getPopularity());
			obj.put("numberOfVideos", playlists.get(i).getNumVideos());
			obj.put("date", playlists.get(i).getDateOfCreation());
			
			JSONArray videos = new JSONArray();
			JSONArray genres = new JSONArray();
			JSONArray moods = new JSONArray();
				
			Iterator<HasVideo> iterator = playlists.get(i).getHasVideos().iterator();
			Iterator<HasGenre> iterator2 = playlists.get(i).getHasGenres().iterator();
			Iterator<HasMood> iterator3 = playlists.get(i).getHasMood().iterator();
				
			while(iterator.hasNext()) {
				Video v = iterator.next().getVideo();
					
				JSONObject video = new JSONObject();
				video.put("name", v.getVideoTitle());
				video.put("description", v.getVideoDescription());
				video.put("url", v.getVideoEmbed());
				video.put("image", v.getVideoImage());
				
				videos.put(video);
			}
				
			while(iterator2.hasNext()) {
				Genre g = iterator2.next().getGenre();
				
				JSONObject genre = new JSONObject();
				genre.put("name", g.getGenreName());
				
				genres.put(genre);
			}
				
			while(iterator3.hasNext()) {
				Mood m = iterator3.next().getMood();
				
				JSONObject mood = new JSONObject();
				mood.put("name", m.getMoodName());
				
				moods.put(mood);
			}
			
			obj.put("videos", videos);
			obj.put("genres", genres);
			obj.put("moods", moods);
				
			array.put(obj);
		}
		
		return array;
	}
}