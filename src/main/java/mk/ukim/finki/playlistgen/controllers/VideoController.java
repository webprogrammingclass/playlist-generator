package mk.ukim.finki.playlistgen.controllers;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import mk.ukim.finki.playlistgen.model.Genre;
import mk.ukim.finki.playlistgen.model.Mood;
import mk.ukim.finki.playlistgen.services.VideoService;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/api/videos")
public class VideoController {
	@Autowired
	VideoService videoService;
	
	@RequestMapping(value="/random", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String getUserPlaylist() {
		String data = videoService.getRandom();

		return data;
	}
	
	@RequestMapping(value="/create/{q}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String getVideos(@PathVariable(value="q") String query, HttpServletRequest request, HttpServletResponse response) {
		String data  = videoService.getVideosByQuery(query);
		
		return data;
	}
	
	@RequestMapping(value="/generate", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ResponseBody
	public String getVideosByGenresAndMoods(@RequestParam("genres") String genres, @RequestParam("moods") String moods, HttpServletRequest request, HttpServletResponse response) throws JSONException {
		List<Genre> tmpGenres = new ArrayList<Genre>();
		List<Mood> tmpMoods = new ArrayList<Mood>();
		
		String[] strGenres = genres.split(",");
		String[] strMoods = moods.split(",");
		
		for(int i=0; i<strGenres.length; i++)
			tmpGenres.add(new Genre(strGenres[i]));
		for(int i=0; i<strMoods.length; i++)
			tmpMoods.add(new Mood(strMoods[i]));
		
		String data = videoService.getVideosByGenresAndMoods(tmpGenres, tmpMoods);
		//System.out.println(data);
		JSONObject videos = new JSONObject();
		videos.put("videos", data);
		
		return videos.toString();
	}
}