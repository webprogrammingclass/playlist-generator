package mk.ukim.finki.playlistgen.security;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.crypto.codec.Hex;

public class TokenUtils {
	public static final String MAGIC_KEY = "obfuscate";

	public static JSONObject createToken(UserDetails userDetails) throws JSONException {
		JSONObject token = new JSONObject();
		
		token.put("username", userDetails.getUsername());
		token.put("key", TokenUtils.computeSignature(userDetails));
		token.put("roles", userDetails.getAuthorities());

		return token;
	}

	public static String computeSignature(UserDetails userDetails) {
		StringBuilder signatureBuilder = new StringBuilder();
		signatureBuilder.append(userDetails.getUsername());
		signatureBuilder.append(":");
		signatureBuilder.append(userDetails.getPassword());
		signatureBuilder.append(":");
		signatureBuilder.append(TokenUtils.MAGIC_KEY);
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			throw new IllegalStateException("No MD5 algorithm available!");
		}

		return new String(Hex.encode(digest.digest(signatureBuilder.toString().getBytes())));
	}

	public static String getUserNameFromToken(String authToken) {
		if (null == authToken) {
			return null;
		}
		String[] parts = authToken.split(":");
		return parts[0];
	}

	public static boolean validateToken(String authToken, UserDetails userDetails) {
		String[] parts = authToken.split(":");
		// long expires = Long.parseLong(parts[1]);
		String signature = parts[1];
		/*
		 * if (expires < System.currentTimeMillis()) { return false; }
		 */
		return signature.equals(TokenUtils.computeSignature(userDetails));
	}
}
