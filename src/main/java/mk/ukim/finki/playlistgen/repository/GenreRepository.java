package mk.ukim.finki.playlistgen.repository;

import java.util.List;

import mk.ukim.finki.playlistgen.model.Genre;

import org.springframework.data.jpa.repository.JpaRepository;

public interface GenreRepository extends JpaRepository<Genre, Long> {
	public Genre findByGenreNameIgnoreCase(String name);
	
	public List<Genre> findByGenreNameContaining(String query);
}
