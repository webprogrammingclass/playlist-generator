package mk.ukim.finki.playlistgen.repository;


import mk.ukim.finki.playlistgen.model.HasPlaylist;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface HasPlaylistRepository extends JpaRepository<HasPlaylist, Long>{
	public Page<HasPlaylist> findAll(Pageable pageable);

	public Page<HasPlaylist> findByUserID(Long id, Pageable pageable);

	public Page<HasPlaylist> findByUserIDNot(Long userID, Pageable pageable);
}
