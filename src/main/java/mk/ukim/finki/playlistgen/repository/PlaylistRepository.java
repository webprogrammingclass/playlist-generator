package mk.ukim.finki.playlistgen.repository;

import java.util.List;

import mk.ukim.finki.playlistgen.model.HasPlaylist;
import mk.ukim.finki.playlistgen.model.Playlist;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface PlaylistRepository extends JpaRepository<Playlist, Long>{
	
	public Playlist findByPlaylistNameIgnoreCase(String name); 
	
	public Page<Playlist> findAll(Pageable pageable);
	
	public Page<Playlist> findByHasPlaylistsNotIn(List<HasPlaylist> p, Pageable pageable);

	public Page<Playlist> findByPlaylistNameContaining(String query, Pageable pageable);
}
