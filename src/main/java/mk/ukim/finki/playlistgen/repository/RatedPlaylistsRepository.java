package mk.ukim.finki.playlistgen.repository;

import mk.ukim.finki.playlistgen.model.RatedPlaylist;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface RatedPlaylistsRepository extends JpaRepository<RatedPlaylist, Long> {

	Page<RatedPlaylist> findByUserID(Long userID, Pageable pageable);

	RatedPlaylist findByUserIDAndPlaylistID(Long userID, Long playlist_id);

}
