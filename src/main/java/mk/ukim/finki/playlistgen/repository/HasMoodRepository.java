package mk.ukim.finki.playlistgen.repository;

import java.util.List;

import mk.ukim.finki.playlistgen.model.HasMood;

import org.springframework.data.jpa.repository.JpaRepository;

public interface HasMoodRepository extends JpaRepository<HasMood, Long>{
	public List<HasMood> findAllByPlaylistID(Long playlistID);
}
