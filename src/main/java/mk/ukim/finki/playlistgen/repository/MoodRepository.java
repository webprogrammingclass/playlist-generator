package mk.ukim.finki.playlistgen.repository;

import mk.ukim.finki.playlistgen.model.Mood;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MoodRepository extends JpaRepository<Mood, Long> {
	public Mood findByMoodNameIgnoreCase(String name);
}
