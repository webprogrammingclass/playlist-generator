package mk.ukim.finki.playlistgen.repository;

import java.util.List;

import mk.ukim.finki.playlistgen.model.ChooseGenre;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ChooseGenreRepository extends JpaRepository<ChooseGenre, Long> {

	List<ChooseGenre> findByUserID(Long userID);

	ChooseGenre findByUserIDAndGenreID(Long userID, Long genreID);
}
