package mk.ukim.finki.playlistgen.repository;


import mk.ukim.finki.playlistgen.model.Video;

import org.springframework.data.jpa.repository.JpaRepository;

public interface VideoRepository extends JpaRepository<Video, Long> {
	
	public Video findByVideoTitleIgnoreCase(String name);
}
