package mk.ukim.finki.playlistgen.repository;

import mk.ukim.finki.playlistgen.model.Authority;

import org.springframework.data.jpa.repository.JpaRepository;

public interface AuthorityRepository extends JpaRepository<Authority, Long> {
	public Authority findByAuthorityName(String string);
	
    public void delete(Authority role);
}