package mk.ukim.finki.playlistgen.repository;

import java.util.List;

import mk.ukim.finki.playlistgen.model.HasGenre;

import org.springframework.data.jpa.repository.JpaRepository;

public interface HasGenreRepository extends JpaRepository<HasGenre, Long>{
	public List<HasGenre> findAllByPlaylistID(Long playlistID);
}
