package mk.ukim.finki.playlistgen.repository;

import java.util.List;

import mk.ukim.finki.playlistgen.model.HasVideo;

import org.springframework.data.jpa.repository.JpaRepository;

public interface HasVideoRepository extends JpaRepository<HasVideo, Long> {
	public List<HasVideo> findAllByPlaylistID(Long playlistID);
}
