package mk.ukim.finki.playlistgen.repository;

import mk.ukim.finki.playlistgen.model.FavoritePlaylist;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FavoritePlaylistsRepository extends JpaRepository<FavoritePlaylist, Long> {

	Page<FavoritePlaylist> findByUserID(Long userID, Pageable pageable);

	FavoritePlaylist findByUserIDAndPlaylistID(Long userID, Long playlist_id);

}
