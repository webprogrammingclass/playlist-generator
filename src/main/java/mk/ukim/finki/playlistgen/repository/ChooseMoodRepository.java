package mk.ukim.finki.playlistgen.repository;

import java.util.List;

import mk.ukim.finki.playlistgen.model.ChooseMood;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ChooseMoodRepository extends JpaRepository<ChooseMood, Long>{

	List<ChooseMood> findByUserID(Long userID);

	ChooseMood findByUserIDAndMoodID(Long userID, Long moodID);

}
