package mk.ukim.finki.playlistgen.repository;

import mk.ukim.finki.playlistgen.model.User;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
	
    public User findByEmail(String email);
    
    public User findByUserName(String username);
    
    public User findByPasswordAndUserName(String password, String username);
}