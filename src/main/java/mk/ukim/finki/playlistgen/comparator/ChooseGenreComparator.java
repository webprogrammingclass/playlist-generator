package mk.ukim.finki.playlistgen.comparator;

import java.util.Comparator;

import mk.ukim.finki.playlistgen.model.ChooseGenre;

public class ChooseGenreComparator implements Comparator<ChooseGenre>{

	@Override
	public int compare(ChooseGenre o1, ChooseGenre o2) {
		if(o1.getCount() > o2.getCount())
			return -1;
		else if(o2.getCount() < o2.getCount())
			return 1;
		else return 0;
	}
}
