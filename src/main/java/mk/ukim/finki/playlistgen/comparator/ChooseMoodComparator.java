package mk.ukim.finki.playlistgen.comparator;

import java.util.Comparator;

import mk.ukim.finki.playlistgen.model.ChooseMood;

public class ChooseMoodComparator implements Comparator<ChooseMood>{

	@Override
	public int compare(ChooseMood o1, ChooseMood o2) {
		if(o1.getCount() > o2.getCount())
			return -1;
		else if(o2.getCount() < o2.getCount())
			return 1;
		else return 0;
	}
}
