package mk.ukim.finki.playlistgen.comparator;

import java.util.Comparator;

import mk.ukim.finki.playlistgen.model.Playlist;

public class PlaylistComparatorByRating implements Comparator<Playlist> {

	@Override
	public int compare(Playlist o1, Playlist o2) {
		if(o1.getRating() > o2.getRating())
			return -1;
		else if(o2.getRating() < o2.getRating())
			return 1;
		else return 0;
	}
}
