package mk.ukim.finki.playlistgen.comparator;

import java.util.Comparator;
import java.util.Date;

import mk.ukim.finki.playlistgen.model.Playlist;

public class PlaylistComparatorByDate implements Comparator<Playlist> {

	@SuppressWarnings("deprecation")
	@Override
	public int compare(Playlist arg0, Playlist arg1) {
		Date d1 = new Date(arg0.getDateOfCreation());
		Date d2 = new Date(arg1.getDateOfCreation());
		
		if(d1.getDate() > d2.getDate())
			return -1;
		else if(d1.getDate() < d2.getDate())
			return 1;
		else return 0;
	}

}
