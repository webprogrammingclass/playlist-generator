package mk.ukim.finki.playlistgen.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "hasAuthority")
@IdClass(HasAuthorityKeys.class)
public class HasAuthority {
	@Id
	private Long userID;
	
	@Id
	private Long authorityID;
	
	@ManyToOne
	@JoinColumn(name = "userID", updatable = false, insertable = false)
	private User user;
	
	@ManyToOne
	@JoinColumn(name = "authorityID", updatable = false, insertable = false)
	private Authority authority;
	
	public HasAuthority() {
		
	}
	
	public HasAuthority(User u, Authority a) {
		this.user = u;
		this.authority = a;
	}

	public Long getUserID() {
		return userID;
	}

	public void setUserID(Long userID) {
		this.userID = userID;
	}

	public Long getAuthorityID() {
		return authorityID;
	}

	public void setAuthorityID(Long authorityID) {
		this.authorityID = authorityID;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Authority getAuthority() {
		return authority;
	}

	public void setAuthority(Authority authority) {
		this.authority = authority;
	}
}

@SuppressWarnings("serial")
class HasAuthorityKeys implements Serializable {
	@SuppressWarnings("unused")
	private Long userID;
	@SuppressWarnings("unused")
	private Long authorityID;
}