package mk.ukim.finki.playlistgen.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "chooseGenre")
@IdClass(ChooseGenreKeys.class)
public class ChooseGenre {
	@Id
	private Long userID;
	
	@Id
	private Long genreID;
	
	@Column
	private int count;
	
	@ManyToOne
	@JoinColumn(name = "userID", updatable = false, insertable = false)
	private User user;
	
	@ManyToOne
	@JoinColumn(name = "genreID", updatable = false, insertable = false)
	private Genre genre;
	
	public ChooseGenre() {
		this.count = 1;
	}
	
	public ChooseGenre(User u, Genre genre2) {
		this.genre = genre2;
		this.user = u;
		this.count = 1;
	}

	public Long getUserID() {
		return userID;
	}

	public void setUserID(Long userID) {
		this.userID = userID;
	}

	public Long getGenreID() {
		return genreID;
	}

	public void setGenreID(Long genreID) {
		this.genreID = genreID;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
}

@SuppressWarnings("serial")
class ChooseGenreKeys implements Serializable {
	@SuppressWarnings("unused")
	private Long userID;
	@SuppressWarnings("unused")
	private Long genreID;
}