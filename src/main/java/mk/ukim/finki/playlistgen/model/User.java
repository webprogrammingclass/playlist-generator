package mk.ukim.finki.playlistgen.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Users")
public class User {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long userID;
	
	@Column(unique = true)
	private String userName;
	
	private String email;
	private String password;
	private int age;
	private String name;
	private String surname;
	private int sex;
	private String imageUrl;
	
	@OneToMany(mappedBy = "user", fetch=FetchType.EAGER)
	private Set<HasAuthority> hasAuthorities;
	
	@OneToMany(mappedBy = "user", fetch=FetchType.EAGER)
	private Set<HasPlaylist> hasPlaylists;
	
	@OneToMany(mappedBy = "user", fetch=FetchType.EAGER)
	private Set<FavoritePlaylist> favoritePlaylists;
	
	@OneToMany(mappedBy = "user", fetch=FetchType.EAGER)
	private Set<RatedPlaylist> ratedPlaylists;
	
	@OneToMany(mappedBy = "user", fetch=FetchType.EAGER, cascade = CascadeType.REMOVE, orphanRemoval = true)
	private Set<ChooseGenre> chooseGenres;
	
	@OneToMany(mappedBy = "user", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE, orphanRemoval = true)
	private Set<ChooseMood> chooseMoods;
 	
	public User() {
		this.hasAuthorities = new HashSet<HasAuthority>();
		this.hasPlaylists = new HashSet<HasPlaylist>();
		this.chooseGenres = new HashSet<ChooseGenre>();
		this.chooseMoods = new HashSet<ChooseMood>();
		this.favoritePlaylists = new HashSet<FavoritePlaylist>();
		this.ratedPlaylists = new HashSet<RatedPlaylist>();
	}
	
	public User(String userName, String email, String password, int age, String name, String surname, int sex, String imageUrl) {
		this.userName = userName;
		this.email = email;
		this.password = password;
		this.age = age;
		this.name = name;
		this.surname = surname;
		this.sex = sex;
		this.imageUrl = imageUrl;
		
		this.hasAuthorities = new HashSet<HasAuthority>();
		this.hasPlaylists = new HashSet<HasPlaylist>();
		this.chooseGenres = new HashSet<ChooseGenre>();
		this.chooseMoods = new HashSet<ChooseMood>();
		this.favoritePlaylists = new HashSet<FavoritePlaylist>();
		this.ratedPlaylists = new HashSet<RatedPlaylist>();
	}

	public Long getUserID() {
		return userID;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public int getAge() {
		return age;
	}

	public void setAge(int age) {
		this.age = age;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public int getSex() {
		return sex;
	}

	public void setSex(int sex) {
		this.sex = sex;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public Set<HasAuthority> getHasAuthority() {
		return hasAuthorities;
	}

	public void setHasAuthority(Set<HasAuthority> hasRoles) {
		this.hasAuthorities = hasRoles;
	}

	public Set<HasPlaylist> getHasPlaylists() {
		return hasPlaylists;
	}

	public void setHasPlaylists(Set<HasPlaylist> hasPlaylists) {
		this.hasPlaylists = hasPlaylists;
	}

	public Set<ChooseGenre> getChooseGenres() {
		return chooseGenres;
	}

	public void setChooseGenres(Set<ChooseGenre> chooseGenres) {
		this.chooseGenres = chooseGenres;
	}

	public Set<ChooseMood> getChooseMoods() {
		return chooseMoods;
	}

	public void setChooseMoods(Set<ChooseMood> chooseMoods) {
		this.chooseMoods = chooseMoods;
	}

	public Set<FavoritePlaylist> getFavoritesPlaylists() {
		return favoritePlaylists;
	}

	public void setFavoritePlaylists(Set<FavoritePlaylist> favoritesPlaylists) {
		this.favoritePlaylists = favoritesPlaylists;
	}

	public Set<RatedPlaylist> getRatedPlaylists() {
		return ratedPlaylists;
	}

	public void setRatedPlaylists(Set<RatedPlaylist> ratedPlaylists) {
		this.ratedPlaylists = ratedPlaylists;
	}
}