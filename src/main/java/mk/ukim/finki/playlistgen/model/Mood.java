package mk.ukim.finki.playlistgen.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Mood")
public class Mood {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long moodID;
	
	@Column(unique = true, nullable = false)
	private String moodName;
	
	@OneToMany(mappedBy = "mood")
	private Set<ChooseMood> chooseMoods;
	
	@OneToMany(mappedBy = "mood")
	private Set<HasMood> hasMood;
	
	public Mood() {
		this.chooseMoods = new HashSet<ChooseMood>();
		this.hasMood = new HashSet<HasMood>();
	}
	
	public Mood(String moodName) {
		this.moodName = moodName;
		
		this.chooseMoods = new HashSet<ChooseMood>();
		this.hasMood = new HashSet<HasMood>();
	}

	public Long getMoodID() {
		return moodID;
	}

	public void setMoodID(Long moodID) {
		this.moodID = moodID;
	}

	public String getMoodName() {
		return moodName;
	}

	public void setMoodName(String moodName) {
		this.moodName = moodName;
	}

	public Set<ChooseMood> getChooseMoods() {
		return chooseMoods;
	}

	public void setChooseMoods(Set<ChooseMood> chooseMoods) {
		this.chooseMoods = chooseMoods;
	}

	public Set<HasMood> getHasMood() {
		return hasMood;
	}

	public void setHasMood(Set<HasMood> hasMood) {
		this.hasMood = hasMood;
	}
}
