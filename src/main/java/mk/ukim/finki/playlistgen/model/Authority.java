package mk.ukim.finki.playlistgen.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Authority")
public class Authority {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long authorityID;
	
	private String authorityName;
	
	@OneToMany(mappedBy = "authority")
	private Set<HasAuthority> hasAuthorities;
	
	public Authority() {
		this.hasAuthorities = new HashSet<HasAuthority>();
	}
	
	public Authority(String authorityName) {
		this.authorityName = authorityName;
		
		this.hasAuthorities = new HashSet<HasAuthority>();
	}

	public Long getAuthorityID() {
		return authorityID;
	}

	public void setAuthorityID(Long authorityID) {
		this.authorityID = authorityID;
	}

	public String getAuthorityName() {
		return authorityName;
	}

	public void setAuthorityName(String authorityName) {
		this.authorityName = authorityName;
	}

	public Set<HasAuthority> getHasAuthorities() {
		return hasAuthorities;
	}

	public void setHasAuthorities(Set<HasAuthority> hasAuthorities) {
		this.hasAuthorities = hasAuthorities;
	}
}
