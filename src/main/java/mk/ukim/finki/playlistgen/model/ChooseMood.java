package mk.ukim.finki.playlistgen.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "chooseMood")
@IdClass(ChooseMoodKeys.class)
public class ChooseMood {
	@Id
	private Long userID;
	
	@Id
	private Long moodID;
	
	@Column
	private int count;
	
	@ManyToOne
	@JoinColumn(name = "userID", updatable = false, insertable = false)
	private User user;
	
	@ManyToOne
	@JoinColumn(name = "moodID", updatable = false, insertable = false)
	private Mood mood;
	
	public ChooseMood() {
		this.count = 1;
		
	}
	
	public ChooseMood(User u, Mood m) {
		this.user = u;
		this.mood = m;
		this.count = 1;
	}

	public Long getUserID() {
		return userID;
	}

	public void setUserID(Long userID) {
		this.userID = userID;
	}

	public Long getMoodID() {
		return moodID;
	}

	public void setMoodID(Long moodID) {
		this.moodID = moodID;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public Mood getMood() {
		return mood;
	}

	public void setMood(Mood mood) {
		this.mood = mood;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}
}

@SuppressWarnings("serial")
class ChooseMoodKeys implements Serializable {
	@SuppressWarnings("unused")
	private Long userID;
	@SuppressWarnings("unused")
	private Long moodID;
}