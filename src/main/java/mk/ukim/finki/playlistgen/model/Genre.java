package mk.ukim.finki.playlistgen.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Genres")
public class Genre {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long genreID;
	
	@Column(unique = true, nullable = false)
	private String genreName;
	
	@OneToMany(mappedBy = "genre")
	private Set<ChooseGenre> chooseGenres;
	
	@OneToMany(mappedBy = "genre")
	private Set<HasGenre> hasGenres;
	
	public Genre() {
		this.chooseGenres = new HashSet<ChooseGenre>();
		this.hasGenres = new HashSet<HasGenre>();
	}
	
	public Genre(String genreName) {
		this.genreName = genreName;
		
		this.chooseGenres = new HashSet<ChooseGenre>();
		this.hasGenres = new HashSet<HasGenre>();
	}

	public Long getGenreID() {
		return genreID;
	}

	public void setGenreID(Long genreID) {
		this.genreID = genreID;
	}

	public String getGenreName() {
		return genreName;
	}

	public void setGenreName(String genreName) {
		this.genreName = genreName;
	}

	public Set<ChooseGenre> getChooseGenres() {
		return chooseGenres;
	}

	public void setChooseGenres(Set<ChooseGenre> chooseGenres) {
		this.chooseGenres = chooseGenres;
	}

	public Set<HasGenre> getHasGenres() {
		return hasGenres;
	}

	public void setHasGenres(Set<HasGenre> hasGenres) {
		this.hasGenres = hasGenres;
	}
}
