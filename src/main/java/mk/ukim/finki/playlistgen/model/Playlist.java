package mk.ukim.finki.playlistgen.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Playlists")
public class Playlist {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long playlistID;
	
	@Column(unique = true, nullable = false)
	private String playlistName;
	
	private int numVideos;
	private float rating;
	private float popularity;
	private String dateOfCreation; 
	private int count;
	
	@OneToMany(mappedBy = "playlist", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE, orphanRemoval = true)
	private Set<HasVideo> hasVideos;

	@OneToMany(mappedBy = "playlist", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE, orphanRemoval = true)
	private Set<HasPlaylist> hasPlaylists;
	
	@OneToMany(mappedBy = "playlist", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE, orphanRemoval = true)
	private Set<HasGenre> hasGenres;
	
	@OneToMany(mappedBy = "playlist", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE, orphanRemoval = true)
	private Set<HasMood> hasMood;
	
	@OneToMany(mappedBy = "playlist", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE, orphanRemoval = true)
	private Set<FavoritePlaylist> favoritePlaylists;
	
	@OneToMany(mappedBy = "playlist", fetch = FetchType.EAGER, cascade = CascadeType.REMOVE, orphanRemoval = true)
	private Set<RatedPlaylist> ratedPlaylists;
	
	public Playlist() {
		this.count = 1;
		
		this.hasPlaylists = new HashSet<HasPlaylist>();
		this.hasVideos = new HashSet<HasVideo>();
		this.hasGenres = new HashSet<HasGenre>();
		this.hasMood = new HashSet<HasMood>();
		this.favoritePlaylists = new HashSet<FavoritePlaylist>();
		this.ratedPlaylists = new HashSet<RatedPlaylist>();
	}
	
	public Playlist(String name, int num, float rating, float popularity, String date) {
		this.playlistName = name;
		this.numVideos = num;
		this.rating = rating;
		this.popularity = popularity;
		this.dateOfCreation = date;
		this.count = 1;
		
		this.hasPlaylists = new HashSet<HasPlaylist>();
		this.hasVideos = new HashSet<HasVideo>();
		this.hasGenres = new HashSet<HasGenre>();
		this.hasMood = new HashSet<HasMood>();
		this.favoritePlaylists = new HashSet<FavoritePlaylist>();
		this.ratedPlaylists = new HashSet<RatedPlaylist>();
	}
	
	public Long getPlaylistId() {
		return playlistID;
	}

	public String getPlaylistName() {
		return playlistName;
	}

	public void setPlaylistName(String playlistName) {
		this.playlistName = playlistName;
	}

	public int getNumVideos() {
		return numVideos;
	}

	public void setNumVideos(int numVideos) {
		this.numVideos = numVideos;
	}

	public float getRating() {
		return rating;
	}

	public void setRating(float rating) {
		this.rating = rating;
	}

	public float getPopularity() {
		return popularity;
	}

	public void setPopularity(float popularity) {
		this.popularity = popularity;
	}

	public String getDateOfCreation() {
		return dateOfCreation;
	}

	public void setDateOfCreation(String dateOfCreation) {
		this.dateOfCreation = dateOfCreation;
	}
	
	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public Long getPlaylistID() {
		return playlistID;
	}

	public void setPlaylistID(Long playlistID) {
		this.playlistID = playlistID;
	}

	public Set<HasPlaylist> getHasPlaylist() {
		return hasPlaylists;
	}

	public void setHasPlaylist(Set<HasPlaylist> hasPlaylists) {
		this.hasPlaylists = hasPlaylists;
	}

	public Set<HasVideo> getHasVideos() {
		return hasVideos;
	}

	public void setHasVideos(Set<HasVideo> hasVideos) {
		this.hasVideos = hasVideos;
	}

	public Set<HasGenre> getHasGenres() {
		return hasGenres;
	}

	public void setHasGenres(Set<HasGenre> hasGenres) {
		this.hasGenres = hasGenres;
	}

	public Set<HasMood> getHasMood() {
		return hasMood;
	}

	public void setHasMood(Set<HasMood> hasMood) {
		this.hasMood = hasMood;
	}

	public Set<FavoritePlaylist> getFavoritePlaylists() {
		return favoritePlaylists;
	}

	public void setFavoritePlaylists(Set<FavoritePlaylist> favoritePlaylists) {
		this.favoritePlaylists = favoritePlaylists;
	}

	public Set<RatedPlaylist> getRatedPlaylists() {
		return ratedPlaylists;
	}

	public void setRatedPlaylists(Set<RatedPlaylist> ratedPlaylists) {
		this.ratedPlaylists = ratedPlaylists;
	}
}
