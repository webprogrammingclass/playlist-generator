package mk.ukim.finki.playlistgen.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "hasPlaylist")
@IdClass(HasPlaylistKeys.class)
public class HasPlaylist {
	@Id
	private Long playlistID;
	
	@Id
	private Long userID;
	
	@ManyToOne
	@JoinColumn(name = "playlistID", updatable = false, insertable = false)
	private Playlist playlist;
	
	@ManyToOne
	@JoinColumn(name = "userID", updatable = false, insertable = false)
	private User user;
	
	public HasPlaylist() {
		
	}
	
	public HasPlaylist(Playlist p, User u) {
		this.playlist = p;
		this.user = u;
	}

	public Long getPlaylistID() {
		return playlistID;
	}

	public void setPlaylistID(Long playlistID) {
		this.playlistID = playlistID;
	}

	public Long getUserID() {
		return userID;
	}

	public void setUserID(Long userID) {
		this.userID = userID;
	}

	public Playlist getPlaylist() {
		return playlist;
	}

	public void setPlaylist(Playlist playlist) {
		this.playlist = playlist;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
}

@SuppressWarnings("serial")
class HasPlaylistKeys implements Serializable {
	@SuppressWarnings("unused")
	private Long playlistID;
	@SuppressWarnings("unused")
	private Long userID;
}