package mk.ukim.finki.playlistgen.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "Videos")
public class Video {
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long videoID;

	private String videoTitle;	
	private String videoEmbed;
	private String videoDescription;
	private String videoImage;
	
	@OneToMany(mappedBy = "video", fetch = FetchType.EAGER)
	private Set<HasVideo> hasVideos;
	
	public Video() {
		this.hasVideos = new HashSet<HasVideo>(); 
	}
	
	public Video(String videoEmbed, String title,String description, String image) {
		this.videoEmbed = videoEmbed;
		this.videoTitle = title;
		this.videoImage = image;
		this.videoDescription = description;
		
		this.hasVideos = new HashSet<HasVideo>();
	}

	public Long getVideoID() {
		return videoID;
	}

	public String getVideoEmbed() {
		return videoEmbed;
	}

	public void setVideoEmbed(String videoEmbed) {
		this.videoEmbed = videoEmbed;
	}

	public String getVideoTitle() {
		return videoTitle;
	}

	public void setVideoTitle(String videoTitle) {
		this.videoTitle = videoTitle;
	}

	public String getVideoDescription() {
		return videoDescription;
	}

	public void setVideoDescription(String videoDescription) {
		this.videoDescription = videoDescription;
	}

	public String getVideoImage() {
		return videoImage;
	}

	public void setVideoImage(String videoImage) {
		this.videoImage = videoImage;
	}
}
