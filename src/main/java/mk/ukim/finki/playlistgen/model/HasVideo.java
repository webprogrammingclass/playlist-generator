package mk.ukim.finki.playlistgen.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "hasVideo")
@IdClass(HasVideoKeys.class)
public class HasVideo {
	@Id
	private Long playlistID;
	
	@Id
	private Long videoID;
	
	@ManyToOne
	@JoinColumn(name = "playlistID", updatable = false, insertable = false)
	private Playlist playlist;
	
	@ManyToOne
	@JoinColumn(name = "videoID", updatable = false, insertable = false)
	private Video video;
	
	public HasVideo() {
		
	}
	
	public HasVideo(Playlist playlist, Video video) {
		this.playlist = playlist;
		this.video = video;
	}

	public Long getPlaylistID() {
		return playlistID;
	}

	public void setPlaylistID(Long playlistID) {
		this.playlistID = playlistID;
	}

	public Long getVideoID() {
		return videoID;
	}

	public void setVideoID(Long videoID) {
		this.videoID = videoID;
	}

	public Playlist getPlaylist() {
		return playlist;
	}

	public void setPlaylist(Playlist playlist) {
		this.playlist = playlist;
	}

	public Video getVideo() {
		return video;
	}

	public void setVideo(Video video) {
		this.video = video;
	}
}

@SuppressWarnings("serial")
class HasVideoKeys implements Serializable {
	@SuppressWarnings("unused")
	private Long playlistID;
	@SuppressWarnings("unused")
	private Long videoID;
}