package mk.ukim.finki.playlistgen.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "hasMood")
@IdClass(HasMoodKeys.class)
public class HasMood {
	@Id
	private Long playlistID;
	
	@Id
	private Long moodID;
	
	@ManyToOne
	@JoinColumn(name = "playlistID", updatable = false, insertable = false)
	private Playlist playlist;
	
	@ManyToOne
	@JoinColumn(name = "moodID", updatable = false, insertable = false)
	private Mood mood;
	
	public HasMood() {
		
	}
	
	public HasMood(Playlist p, Mood m) {
		this.playlist = p;
		this.mood = m;
	}

	public Long getPlaylistID() {
		return playlistID;
	}

	public void setPlaylistID(Long playlistID) {
		this.playlistID = playlistID;
	}

	public Long getMoodID() {
		return moodID;
	}

	public void setMoodID(Long moodID) {
		this.moodID = moodID;
	}

	public Playlist getPlaylist() {
		return playlist;
	}

	public void setPlaylist(Playlist playlist) {
		this.playlist = playlist;
	}

	public Mood getMood() {
		return mood;
	}

	public void setMood(Mood mood) {
		this.mood = mood;
	}
}

@SuppressWarnings("serial")
class HasMoodKeys implements Serializable {
	@SuppressWarnings("unused")
	private Long playlistID;
	@SuppressWarnings("unused")
	private Long moodID;
}