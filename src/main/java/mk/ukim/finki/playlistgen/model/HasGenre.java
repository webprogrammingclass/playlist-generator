package mk.ukim.finki.playlistgen.model;

import java.io.Serializable;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.IdClass;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Table(name = "hasGenre")
@IdClass(HasGenreKeys.class)
public class HasGenre {
	@Id
	private Long playlistID;
	
	@Id
	private Long genreID;
	
	@ManyToOne
	@JoinColumn(name = "playlistID", updatable = false, insertable = false)
	private Playlist playlist;
	
	@ManyToOne
	@JoinColumn(name = "genreID", updatable = false, insertable = false)
	private Genre genre;
	
	public HasGenre() {
		
	}
	
	public HasGenre(Playlist p, Genre g) {
		this.playlist = p;
		this.genre = g;
	}
	
	public Long getPlaylistID() {
		return playlistID;
	}

	public void setPlaylistID(Long playlistID) {
		this.playlistID = playlistID;
	}

	public Long getGenreID() {
		return genreID;
	}

	public void setGenreID(Long genreID) {
		this.genreID = genreID;
	}

	public Playlist getPlaylist() {
		return playlist;
	}

	public void setPlaylist(Playlist playlist) {
		this.playlist = playlist;
	}

	public Genre getGenre() {
		return genre;
	}

	public void setGenre(Genre genre) {
		this.genre = genre;
	}
}

class HasGenreKeys implements Serializable {
	private static final long serialVersionUID = 1L;
	@SuppressWarnings("unused")
	private Long playlistID;
	@SuppressWarnings("unused")
	private Long genreID;
}