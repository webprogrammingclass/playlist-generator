package mk.ukim.finki.playlistgen.services;

import java.util.List;

import mk.ukim.finki.playlistgen.model.Mood;

public interface MoodService {
public List<Mood> getAllMoods();
	
	public boolean saveAllMoods(List<Mood> moods);
}
