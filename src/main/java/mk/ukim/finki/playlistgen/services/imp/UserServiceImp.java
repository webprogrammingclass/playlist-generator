package mk.ukim.finki.playlistgen.services.imp;

import java.util.List;

import javax.servlet.http.Cookie;

import mk.ukim.finki.playlistgen.model.Authority;
import mk.ukim.finki.playlistgen.model.ChooseGenre;
import mk.ukim.finki.playlistgen.model.ChooseMood;
import mk.ukim.finki.playlistgen.model.FavoritePlaylist;
import mk.ukim.finki.playlistgen.model.HasAuthority;
import mk.ukim.finki.playlistgen.model.HasPlaylist;
import mk.ukim.finki.playlistgen.model.Playlist;
import mk.ukim.finki.playlistgen.model.RatedPlaylist;
import mk.ukim.finki.playlistgen.model.User;
import mk.ukim.finki.playlistgen.repository.AuthorityRepository;
import mk.ukim.finki.playlistgen.repository.ChooseGenreRepository;
import mk.ukim.finki.playlistgen.repository.ChooseMoodRepository;
import mk.ukim.finki.playlistgen.repository.FavoritePlaylistsRepository;
import mk.ukim.finki.playlistgen.repository.HasAuthorityRepository;
import mk.ukim.finki.playlistgen.repository.HasPlaylistRepository;
import mk.ukim.finki.playlistgen.repository.PlaylistRepository;
import mk.ukim.finki.playlistgen.repository.RatedPlaylistsRepository;
import mk.ukim.finki.playlistgen.repository.UserRepository;
import mk.ukim.finki.playlistgen.services.UserService;
import mk.ukim.finki.playlistgen.validation.EmailExistsException;
import mk.ukim.finki.playlistgen.validation.NoValidatePasswordException;
import mk.ukim.finki.playlistgen.validation.PasswordDoesntExistsException;
import mk.ukim.finki.playlistgen.validation.PasswordsNotMatchException;
import mk.ukim.finki.playlistgen.validation.UserNameExistsException;

import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImp implements UserService {
	private static final String GAVATER = "http://www.gravatar.com/avatar/";
	@Autowired
    private UserRepository userRepository;
	
	@Autowired
	private PlaylistRepository playlistRepository;
	
	@Autowired
    private AuthorityRepository authorityRepository;
	
	@Autowired
	private HasAuthorityRepository hasAuthorityRepository;
	
	@Autowired
	private ChooseGenreRepository chooseGenreRepository;
	
	@Autowired
	private ChooseMoodRepository chooseMoodRepository;
	
	@Autowired
	private HasPlaylistRepository hasPlaylistRepository;
	
	@Autowired
	private RatedPlaylistsRepository ratedPlaylistRepository;
	
	@Autowired
	private FavoritePlaylistsRepository favoritePlaylistsRepository;

	@Override
	public User registerNewUserAccount(String username, String email, String password, int ageNumber, String name, String lastname, int hummanity) throws EmailExistsException, UserNameExistsException {
		if (emailExist(email))
            throw new EmailExistsException("There is an account with email adress: " + email);
		else if(userNameExists(username))
			throw new UserNameExistsException("There is an account with username: " + username);
		else {
			//creating new user
			String tmp = email.trim().toLowerCase();
			String hash = md5Spring(tmp);
			
			User user = new User(username, email, password, ageNumber, name, lastname, hummanity, GAVATER + hash + "?s=150");
			userRepository.save(user);
			
			//predefined authority for all users
			Authority a = authorityRepository.findByAuthorityName("ROLE_USERS");
			if(a == null)
				authorityRepository.save(new Authority("ROLE_USERS"));
			
			HasAuthority hasAuthority = new HasAuthority(user, a);
			hasAuthority.setAuthorityID(a.getAuthorityID());
			hasAuthority.setUserID(user.getUserID());
			
			hasAuthorityRepository.save(hasAuthority);
			
			return user;
		}
	}

	@Override
	public String getUserToken(Cookie[] cookies, String verificationToken) throws JSONException {
		if (cookies != null) {
            for (Cookie ck : cookies) {
            	System.out.println(ck.getName() + " " + ck.getValue());
                if(ck.getName().toString().equals(verificationToken) && ck.getValue() != null) {
                	System.out.println("YES");
                	JSONObject json = new JSONObject();
                	JSONObject jsonobj = new JSONObject(ck.getValue());
                	
                	User user = userRepository.findByUserName(jsonobj.getString("username"));
                	String user_info = "{\"userID\":\""+ user.getUserID() + "\",\"name\":\"" + user.getName() + "\",\"surname\":\"" + user.getSurname() + "\",\"email\":\"" + user.getEmail() + "\",\"image\":\"" + user.getImageUrl() + "\"}";
                	
                	json.put("userInfo", user_info);
                	json.put("token", ck.getValue());
                	
	                return json.toString();
                }
            }
		}
		
		return null;
	}
	
	private boolean emailExist(String email) {
        User user = userRepository.findByEmail(email);
        
        return (user != null);
    }
	
	private boolean userNameExists(String username) {
		User user = userRepository.findByUserName(username);
		
		return (user != null);
	}
	
	private boolean passwordExists(String password, String username) {
		User user = userRepository.findByPasswordAndUserName(password, username);
		
		return (user != null);
	}

	@Override
	public boolean changePassword(Long userID, String oldPass, String newPass, String newPassConf) throws PasswordsNotMatchException, PasswordDoesntExistsException {
		if(newPass.equals(newPassConf)) {
			User user = userRepository.findOne(userID);
			
			if(user.getPassword().equals(oldPass)) {
				user.setPassword(newPass);
				
				userRepository.save(user);
				
				return true;
			}
			else 
				throw new PasswordDoesntExistsException("Your old password was incorect!");
		}
		else 
			throw new PasswordsNotMatchException("Your new password doesn't match up with the confirm password!");
	}

	@Override
	public boolean checkUser(String password, String username) throws NoValidatePasswordException {
		if(passwordExists(password, username))
			return true;
		else
			throw new NoValidatePasswordException("Invalid password or username!");
	}
	
	public static String md5Spring(String text){ 
		return DigestUtils.md5Hex(text); 
	}

	@Override
	public Page<HasPlaylist> getUserPlaylists(Long userID, Pageable pageable) {
		return hasPlaylistRepository.findByUserID(userID, pageable);
	}

	@Override
	public Page<RatedPlaylist> getUserRatedPlaylists(Long userID, Pageable pageable) {
		return ratedPlaylistRepository.findByUserID(userID, pageable);
	}

	@Override
	public Page<FavoritePlaylist> getUserFavoritesPlaylists(Long userID, Pageable pageable) {
		return favoritePlaylistsRepository.findByUserID(userID, pageable);
	}

	@Override
	public List<ChooseMood> getUserMoods(Long userID) {
		return chooseMoodRepository.findByUserID(userID);
	}

	@Override
	public List<ChooseGenre> getUserGenres(Long userID) {
		return chooseGenreRepository.findByUserID(userID);
	}

	@Override
	public FavoritePlaylist getUserFavoritePlaylist(Long userID, Long playlist_id) {
		return favoritePlaylistsRepository.findByUserIDAndPlaylistID(userID, playlist_id);
	}

	@Override
	public RatedPlaylist getUserRatedPlaylist(Long userID, Long playlist_id) {
		return ratedPlaylistRepository.findByUserIDAndPlaylistID(userID, playlist_id);
	}

	@Override
	public boolean setUserFavoritePlaylist(Long userID, Long playlist_id) {
		Playlist p = playlistRepository.findOne(playlist_id);
		User u = userRepository.findOne(userID);
		
		if(p != null && u != null) {
			FavoritePlaylist favoritePlaylist = new FavoritePlaylist(p, u);
			favoritePlaylist.setPlaylistID(playlist_id);
			favoritePlaylist.setUserID(userID);
			
			favoritePlaylistsRepository.save(favoritePlaylist);
			
			return true;
		}
		
		return false;
	}

	@Override
	public boolean deleteUserFavoritePlaylist(Long userID, Long playlist_id) {
		FavoritePlaylist favoritePlaylist = favoritePlaylistsRepository.findByUserIDAndPlaylistID(userID, playlist_id);
		
		favoritePlaylistsRepository.delete(favoritePlaylist);
		
		return true;
	}

	@Override
	public boolean setUserRatedPlaylist(Long userID, Long playlist_id, float rating) {
		Playlist p = playlistRepository.findOne(playlist_id);
		User u = userRepository.findOne(userID);
		
		if(p != null && u != null) {
			int count = p.getCount();
			count += 1;
			
			float oldRating = p.getRating();
			rating += oldRating;
			
			p.setCount(count);
			p.setRating(rating / count);
			
			playlistRepository.save(p);
			
			RatedPlaylist ratedPlaylist = new RatedPlaylist(p, u);
			ratedPlaylist.setPlaylistID(playlist_id);
			ratedPlaylist.setUserID(userID);
			
			ratedPlaylistRepository.save(ratedPlaylist);
			
			return true;
		}
		
		return false;
	}
}
