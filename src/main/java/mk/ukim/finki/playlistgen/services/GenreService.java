package mk.ukim.finki.playlistgen.services;

import java.util.List;

import mk.ukim.finki.playlistgen.model.Genre;

public interface GenreService {
	public List<Genre> getAllGenres();
	
	public boolean saveAllGenres(List<Genre> genres);

	public Genre getGenre(String id);
}
