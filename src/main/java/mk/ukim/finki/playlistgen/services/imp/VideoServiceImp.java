package mk.ukim.finki.playlistgen.services.imp;

import java.util.List;
import java.util.Random;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import mk.ukim.finki.playlistgen.model.Genre;
import mk.ukim.finki.playlistgen.model.Mood;
import mk.ukim.finki.playlistgen.services.VideoService;

@Service
public class VideoServiceImp implements VideoService {
	
	private static final String BASE_URL_SEARCH_ARTISTS = "http://developer.echonest.com/api/v4/artist/search?results=99&format=json&sort=familiarity-desc";
	private static final String BASE_URL_SEARCH_VIDEOS = "https://www.googleapis.com/youtube/v3/search?part=snippet&type=video&videoCategoryId=10&videoEmbeddable=true&maxResults=20";
	private static final String BASE_URL_SEARCH_ARTISTS_VIDEOS = "https://www.googleapis.com/youtube/v3/search?part=snippet&type=video&videoCategoryId=10&videoEmbeddable=true&order=viewCount&maxResults=20";
	
	private static final String YOUTUBE_API_KEY = "AIzaSyCyciu3G4iAikwIhd83mtM44nVjlupkUCM";
	private static final String ECHONEST_API_KEY = "SL6Y54LFM7CFILKOS";
	
	private RestTemplate restTemplate;
	
	public VideoServiceImp() {
		restTemplate = new RestTemplate();
	}
	
	@Override
	public String getRandom() {
		return restTemplate.getForObject(BASE_URL_SEARCH_VIDEOS + "&q=house&maxResults=1&key=" + YOUTUBE_API_KEY, String.class);
	}

	@Override
	public String getVideosByQuery(String q) {
		return restTemplate.getForObject(BASE_URL_SEARCH_VIDEOS + "&q=" + q + "&key=" + YOUTUBE_API_KEY, String.class);
	}

	@Override
	public String getVideosByGenresAndMoods(List<Genre> genres, List<Mood> moods) throws JSONException {
		StringBuilder tmpGenres = new StringBuilder();
		StringBuilder tmpMoods = new StringBuilder();
		
		for(int i=0; i<genres.size(); i++)
			tmpGenres.append("&style=" + genres.get(i).getGenreName());
		
		for(int i=0; i<moods.size(); i++)
			tmpMoods.append("&mood=" + moods.get(i).getMoodName() + "^" + (moods.size()-i));
		
		//random country of the artists
		String country = "";
		Random randomCountry = new Random();
		if(randomCountry.nextInt(1) == 1)
			country = "united states";
		else
			country = "united kingdom";
		
		//artists that have given genres and moods
		String response = restTemplate.getForObject(BASE_URL_SEARCH_ARTISTS + "&artist_location=country:" + country + "&api_key=" + ECHONEST_API_KEY + tmpGenres.toString() + tmpMoods.toString(), String.class);
		
		JSONObject json = new JSONObject(response);
		JSONArray tmpArtists = json.getJSONObject("response").getJSONArray("artists");
		
		String[] strings = new String[tmpArtists.length()];
		String[] artists = new String[10];
		
		for(int i=0; i<tmpArtists.length(); i++) {
			JSONObject artist = tmpArtists.getJSONObject(i);
			strings[i] = artist.getString("name");
		}
		
		strings = shuffleArray(strings);
		
		for(int i=0; i<9; i++) {
			artists[i] = strings[i];
			//System.out.println(artists[i]);
		}
		
		return this.getVideosByArtists(artists);
	}
	
	private String[] shuffleArray(String[] ar) {
	    Random rnd = new Random();
	    for (int i = ar.length - 1; i > 0; i--) {
	    	int index = rnd.nextInt(i + 1);
	    	// Simple swap
	    	String a = ar[index];
	    	ar[index] = ar[i];
	    	ar[i] = a;
	    }
	    
	    return ar;
	}
	
	private String getVideosByArtists(String[] artists) throws JSONException {
		StringBuilder videos = new StringBuilder();
		videos.append("[");
		
		for(int i=0; i<artists.length; i++) {
			String s = restTemplate.getForObject(BASE_URL_SEARCH_ARTISTS_VIDEOS + "&q=" + artists[i] + "&key=" + YOUTUBE_API_KEY, String.class);
			
			JSONObject json_videos = new JSONObject(s);
			JSONArray array_videos = json_videos.getJSONArray("items");
			
			Random randomVideo = new Random();
			int j = randomVideo.nextInt(19);
			
			if(!array_videos.isNull(j))
				videos.append(array_videos.getJSONObject(j).toString() + ",");
			
			int k = randomVideo.nextInt(19);
			if(j == k) {
				if(k <= 18 && k >= 1)
					k = k - 1;
				else
					k = 10;
			}
			
			if(!array_videos.isNull(k)) {
				if(i < artists.length-1)
					videos.append(array_videos.getJSONObject(k).toString() + ",");
				else 
					videos.append(array_videos.getJSONObject(k).toString());
			}
		}
		
		videos.append("]");
		
		return videos.toString();
	}
}
