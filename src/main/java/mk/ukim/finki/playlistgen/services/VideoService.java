package mk.ukim.finki.playlistgen.services;

import java.util.List;

import org.json.JSONException;

import mk.ukim.finki.playlistgen.model.Genre;
import mk.ukim.finki.playlistgen.model.Mood;

public interface VideoService {
	public String getRandom();
	
	public String getVideosByQuery(String q);

	public String getVideosByGenresAndMoods(List<Genre> tmpGenres, List<Mood> tmpMoods) throws JSONException;
}