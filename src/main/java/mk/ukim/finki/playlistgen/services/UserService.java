package mk.ukim.finki.playlistgen.services;

import java.util.List;

import javax.servlet.http.Cookie;

import org.json.JSONException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import mk.ukim.finki.playlistgen.model.ChooseGenre;
import mk.ukim.finki.playlistgen.model.ChooseMood;
import mk.ukim.finki.playlistgen.model.FavoritePlaylist;
import mk.ukim.finki.playlistgen.model.HasPlaylist;
import mk.ukim.finki.playlistgen.model.RatedPlaylist;
import mk.ukim.finki.playlistgen.model.User;
import mk.ukim.finki.playlistgen.validation.EmailExistsException;
import mk.ukim.finki.playlistgen.validation.NoValidatePasswordException;
import mk.ukim.finki.playlistgen.validation.PasswordDoesntExistsException;
import mk.ukim.finki.playlistgen.validation.PasswordsNotMatchException;
import mk.ukim.finki.playlistgen.validation.UserNameExistsException;

public interface UserService {
	public User registerNewUserAccount(String username, String email, String password, int ageNumber, String name, String lastname, int hummanity) throws EmailExistsException, UserNameExistsException;

    public String getUserToken(Cookie[] c, String verificationToken) throws JSONException;

	public boolean changePassword(Long userID, String oldPass, String newPass, String newPassConf) throws PasswordsNotMatchException, PasswordDoesntExistsException;

	public boolean checkUser(String password, String username) throws NoValidatePasswordException;
	
	public Page<HasPlaylist> getUserPlaylists(Long userID, Pageable pageable);
	
	public Page<RatedPlaylist> getUserRatedPlaylists(Long userID, Pageable pageable);
	
	public Page<FavoritePlaylist> getUserFavoritesPlaylists(Long userID, Pageable pageable);
	
	public List<ChooseMood> getUserMoods(Long userID);
	
	public List<ChooseGenre> getUserGenres(Long userID);

	public FavoritePlaylist getUserFavoritePlaylist(Long userID, Long playlist_id);
	
	public RatedPlaylist getUserRatedPlaylist(Long userID, Long playlist_id);

	public boolean setUserFavoritePlaylist(Long userID, Long playlist_id);
	
	public boolean deleteUserFavoritePlaylist(Long userID, Long playlist_id);

	boolean setUserRatedPlaylist(Long userID, Long playlist_id, float rating);
}
