package mk.ukim.finki.playlistgen.services.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mk.ukim.finki.playlistgen.model.Mood;
import mk.ukim.finki.playlistgen.repository.MoodRepository;
import mk.ukim.finki.playlistgen.services.MoodService;

@Service
public class MoodServiceImp implements MoodService {
	@Autowired
	private MoodRepository moodRepository;
	
	@Override
	public List<Mood> getAllMoods() {
		return moodRepository.findAll();
	}

	@Override
	public boolean saveAllMoods(List<Mood> moods) {
		for(int i=0; i<moods.size(); i++) {
			Mood m = moodRepository.findByMoodNameIgnoreCase(moods.get(i).getMoodName());
			if(m == null)
				moodRepository.save(moods.get(i));
		}
		
		return true;
	}

}
