package mk.ukim.finki.playlistgen.services.imp;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import mk.ukim.finki.playlistgen.comparator.PlaylistComparatorByDate;
import mk.ukim.finki.playlistgen.comparator.PlaylistComparatorByRating;
import mk.ukim.finki.playlistgen.model.ChooseGenre;
import mk.ukim.finki.playlistgen.model.ChooseMood;
import mk.ukim.finki.playlistgen.model.Genre;
import mk.ukim.finki.playlistgen.model.HasGenre;
import mk.ukim.finki.playlistgen.model.HasMood;
import mk.ukim.finki.playlistgen.model.HasPlaylist;
import mk.ukim.finki.playlistgen.model.HasVideo;
import mk.ukim.finki.playlistgen.model.Mood;
import mk.ukim.finki.playlistgen.model.Playlist;
import mk.ukim.finki.playlistgen.model.User;
import mk.ukim.finki.playlistgen.model.Video;
import mk.ukim.finki.playlistgen.repository.ChooseGenreRepository;
import mk.ukim.finki.playlistgen.repository.ChooseMoodRepository;
import mk.ukim.finki.playlistgen.repository.GenreRepository;
import mk.ukim.finki.playlistgen.repository.HasGenreRepository;
import mk.ukim.finki.playlistgen.repository.HasMoodRepository;
import mk.ukim.finki.playlistgen.repository.HasPlaylistRepository;
import mk.ukim.finki.playlistgen.repository.HasVideoRepository;
import mk.ukim.finki.playlistgen.repository.MoodRepository;
import mk.ukim.finki.playlistgen.repository.PlaylistRepository;
import mk.ukim.finki.playlistgen.repository.UserRepository;
import mk.ukim.finki.playlistgen.repository.VideoRepository;
import mk.ukim.finki.playlistgen.services.PlaylistService;
import mk.ukim.finki.playlistgen.validation.PlaylistExistsException;

@Service
public class PlaylistServiceImp implements PlaylistService {
	@Autowired
	private HasVideoRepository hasVideoRepository;
	
	@Autowired
	private HasGenreRepository hasGenreRepository;
	
	@Autowired
	private HasMoodRepository hasMoodRepository;
	
	@Autowired
	private PlaylistRepository playlistRepository;
	
	@Autowired
	private VideoRepository videoRepository;
	
	@Autowired
	private MoodRepository moodRepository;
	
	@Autowired
	private GenreRepository genreRepository;
	
	@Autowired
	private ChooseGenreRepository chooseGenreRepository;
	
	@Autowired
	private ChooseMoodRepository chooseMoodRepository;
	
	@Autowired
	private HasPlaylistRepository hasPlaylistRepository;
	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public Playlist createPlaylist(Long userID, String name, List<Video> videos, List<Mood> moods, List<Genre> genres) throws PlaylistExistsException {
		if(playlistRepository.findByPlaylistNameIgnoreCase(name) != null)
			throw new PlaylistExistsException("There is an existing playlist with name " + name);
		else {
			DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
			Date date = new Date();
			
			//creating new playlist
			Playlist playlist = new Playlist(name, videos.size(), 0, 0, dateFormat.format(date));
			
			playlistRepository.save(playlist);
			
			User u = userRepository.findOne(userID);
			
			HasPlaylist hasPlaylist = new HasPlaylist(playlist, u);
			hasPlaylist.setPlaylistID(playlist.getPlaylistId());
			hasPlaylist.setUserID(userID);
			
			hasPlaylistRepository.save(hasPlaylist);
			
			for(int i=0; i<videos.size(); i++) {
				Video video = videoRepository.findByVideoTitleIgnoreCase(videos.get(i).getVideoTitle());
				HasVideo hasVideo = new HasVideo();
				
				if(video == null) {
					videoRepository.save(videos.get(i));
					
					hasVideo.setVideo(videos.get(i));
					hasVideo.setVideoID(videos.get(i).getVideoID());
				}
				else {
					hasVideo.setVideo(video);
					hasVideo.setVideoID(video.getVideoID());
				}
				
				hasVideo.setPlaylist(playlist);
				hasVideo.setPlaylistID(playlist.getPlaylistId());
				
				hasVideoRepository.save(hasVideo);
			}
			
			for(int i=0; i<moods.size(); i++) {
				Mood mood = moodRepository.findByMoodNameIgnoreCase(moods.get(i).getMoodName());
				HasMood hasMood = new HasMood();
				
				if(mood == null) {
					moodRepository.save(moods.get(i));
					
					hasMood.setMood(moods.get(i));
					hasMood.setMoodID(moods.get(i).getMoodID());
				}
				else {
					hasMood.setMood(mood);
					hasMood.setMoodID(mood.getMoodID());
				}
				
				hasMood.setPlaylist(playlist);
				hasMood.setPlaylistID(playlist.getPlaylistId());
				
				hasMoodRepository.save(hasMood);
			}
			
			for(int i=0; i<genres.size(); i++) {
				Genre genre = genreRepository.findByGenreNameIgnoreCase(genres.get(i).getGenreName());
				HasGenre hasGenre = new HasGenre();
				
				if(genre == null) {
					genreRepository.save(genres.get(i));
					
					hasGenre.setGenre(genres.get(i));
					hasGenre.setGenreID(genres.get(i).getGenreID());
				}
				else {
					hasGenre.setGenre(genre);
					hasGenre.setGenreID(genre.getGenreID());
				}
				
				hasGenre.setPlaylist(playlist);
				hasGenre.setPlaylistID(playlist.getPlaylistId());
				
				hasGenreRepository.save(hasGenre);
			}
			
			return playlist;
		}
	}
	
	@Override
	public Playlist generatePlaylist(Long userID, String playlistName, List<Video> videos, List<Mood> moods, List<Genre> genres) throws PlaylistExistsException {
		//creating new playlist
		Playlist p = this.createPlaylist(userID, playlistName, videos, moods, genres);
		User u = userRepository.findOne(userID);
		
		for(int i=0; i<moods.size(); i++) {
			Mood mood = moodRepository.findByMoodNameIgnoreCase(moods.get(i).getMoodName());
			ChooseMood chooseMood = chooseMoodRepository.findByUserIDAndMoodID(userID, moods.get(i).getMoodID());
			
			if(chooseMood == null) {
				chooseMood = new ChooseMood(u, mood);
				
				chooseMood.setUserID(userID);
				chooseMood.setMoodID(mood.getMoodID());
			}
			else {
				int count = chooseMood.getCount();
				count += 1;
				
				chooseMood.setCount(count);
			}
			
			chooseMoodRepository.save(chooseMood);
		}
		
		for(int i=0; i<genres.size(); i++) {
			Genre genre = genreRepository.findByGenreNameIgnoreCase(genres.get(i).getGenreName());
			ChooseGenre chooseGenre = chooseGenreRepository.findByUserIDAndGenreID(userID, genres.get(i).getGenreID());
			
			if(chooseGenre == null) {
				chooseGenre = new ChooseGenre(u, genre);
				
				chooseGenre.setUserID(userID);
				chooseGenre.setGenreID(genre.getGenreID());
			}
			else {
				int count = chooseGenre.getCount();
				count += 1;
				
				chooseGenre.setCount(count);
			}
			
			chooseGenreRepository.save(chooseGenre);
		}
		
		return p;
	}

	@Override
	public boolean removePlaylist(Long playlistID) {
		Playlist playlist = playlistRepository.findOne(playlistID);
		playlistRepository.delete(playlist);
		
		return true;
	}

	@Override
	public Playlist editPlaylist(Long playlistID, String playlistName, List<Video> videos, List<Mood> moods, List<Genre> genres, boolean isNewName) throws PlaylistExistsException {
		if(isNewName)
			if(playlistRepository.findByPlaylistNameIgnoreCase(playlistName) != null)
				throw new PlaylistExistsException("There is an existing playlist with name " + playlistName);
		
		Playlist playlist = playlistRepository.findOne(playlistID);
		playlist.setPlaylistName(playlistName);
		
		playlistRepository.save(playlist);
		
		List<HasGenre> hasGenres = hasGenreRepository.findAllByPlaylistID(playlistID);
		List<HasMood> hasMoods = hasMoodRepository.findAllByPlaylistID(playlistID);
		List<HasVideo> hasVideos = hasVideoRepository.findAllByPlaylistID(playlistID);
		
		for(int i=0; i<hasGenres.size(); i++) 
			hasGenreRepository.delete(hasGenres.get(i));
		
		for(int i=0; i<hasMoods.size(); i++) 
			hasMoodRepository.delete(hasMoods.get(i));
		
		for(int i=0; i<hasVideos.size(); i++)
			hasVideoRepository.delete(hasVideos.get(i));
		
		
		for(int i=0; i<videos.size(); i++) {
			Video video = videoRepository.findByVideoTitleIgnoreCase(videos.get(i).getVideoTitle());
			HasVideo hasVideo = new HasVideo();
			
			if(video == null) {
				videoRepository.save(videos.get(i));
				
				hasVideo.setVideo(videos.get(i));
				hasVideo.setVideoID(videos.get(i).getVideoID());
			}
			else {
				hasVideo.setVideo(video);
				hasVideo.setVideoID(video.getVideoID());
			}
			
			hasVideo.setPlaylist(playlist);
			hasVideo.setPlaylistID(playlist.getPlaylistId());
			
			hasVideoRepository.save(hasVideo);
		}
		
		for(int i=0; i<moods.size(); i++) {
			Mood mood = moodRepository.findByMoodNameIgnoreCase(moods.get(i).getMoodName());
			HasMood hasMood = new HasMood();
			
			if(mood == null) {
				moodRepository.save(moods.get(i));
				
				hasMood.setMood(moods.get(i));
				hasMood.setMoodID(moods.get(i).getMoodID());
			}
			else {
				hasMood.setMood(mood);
				hasMood.setMoodID(mood.getMoodID());
			}
			
			hasMood.setPlaylist(playlist);
			hasMood.setPlaylistID(playlist.getPlaylistId());
			
			hasMoodRepository.save(hasMood);
		}
		
		for(int i=0; i<genres.size(); i++) {
			Genre genre = genreRepository.findByGenreNameIgnoreCase(genres.get(i).getGenreName());
			HasGenre hasGenre = new HasGenre();
			
			if(genre == null) {
				genreRepository.save(genres.get(i));
				
				hasGenre.setGenre(genres.get(i));
				hasGenre.setGenreID(genres.get(i).getGenreID());
			}
			else {
				hasGenre.setGenre(genre);
				hasGenre.setGenreID(genre.getGenreID());
			}
			
			hasGenre.setPlaylist(playlist);
			hasGenre.setPlaylistID(playlist.getPlaylistId());
			
			hasGenreRepository.save(hasGenre);
		}
		System.out.println(playlist.getPlaylistName());
		return playlist;
	}

	@Override
	public Page<HasPlaylist> getAdminPlaylists(Long userID, Pageable pageable) {
		Page<HasPlaylist> hasAdminPlaylists = hasPlaylistRepository.findByUserID(userID, pageable);
		
		return hasAdminPlaylists;
	}

	@Override
	public Page<HasPlaylist> getUsersPlaylists(Long userID, Pageable pageable) {
		Page<HasPlaylist> hasUsersPlaylists = hasPlaylistRepository.findByUserIDNot(userID, pageable);
		
		return hasUsersPlaylists;
	}	

	@Override
	public Playlist getPlaylist(Long id) {
		return playlistRepository.findOne(id);
	}

	@Override
	public Page<Playlist> getAdminPlaylistsFromSearch(String query, Pageable pageable) {
		Page<Playlist> playlists = playlistRepository.findByPlaylistNameContaining(query, pageable);
		
		return playlists;
	}

	@Override
	public Page<Playlist> getAllPlaylists(Pageable pageable) {
		return playlistRepository.findAll(pageable);
	}

	@Override
	public List<Playlist> getNewestPlaylists() {
		List<Playlist> allPlaylists = playlistRepository.findAll();
		List<Playlist> playlists = new ArrayList<Playlist>();
		
		Collections.sort(allPlaylists, new PlaylistComparatorByDate());
		for(int i=0; i<3; i++) {
			playlists.add(allPlaylists.get(i));
			//System.out.println(playlists.get(i).getDateOfCreation() + " " + playlists.get(i).getPlaylistName());
		}
		return playlists;
	}

	@Override
	public List<Playlist> getTopRatedPlaylists() {
		List<Playlist> allPlaylists = playlistRepository.findAll();
		List<Playlist> playlists = new ArrayList<Playlist>();
		
		Collections.sort(allPlaylists, new PlaylistComparatorByRating());
		for(int i=0; i<3; i++) {
			playlists.add(allPlaylists.get(i));
			System.out.println("TAG " + playlists.get(i).getRating() + " " + playlists.get(i).getPlaylistName());
		}
		return playlists;
	}
}
