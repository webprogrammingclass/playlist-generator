package mk.ukim.finki.playlistgen.services;

import java.util.List;

import mk.ukim.finki.playlistgen.model.Genre;
import mk.ukim.finki.playlistgen.model.HasPlaylist;
import mk.ukim.finki.playlistgen.model.Mood;
import mk.ukim.finki.playlistgen.model.Playlist;
import mk.ukim.finki.playlistgen.model.Video;
import mk.ukim.finki.playlistgen.validation.PlaylistExistsException;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface PlaylistService {
	public Playlist generatePlaylist(Long userID, String playlistName, List<Video> videos, List<Mood> moods, List<Genre>genres) throws PlaylistExistsException;
	
	public Playlist getPlaylist(Long id);
	
	public boolean removePlaylist(Long playlistID);
	
	public Playlist editPlaylist(Long playlistID, String playlistName, List<Video> videos, List<Mood> moods, List<Genre> genres, boolean isNewName) throws PlaylistExistsException;
	
	public Page<Playlist> getAllPlaylists(Pageable pageable);
	
	public Page<Playlist> getAdminPlaylistsFromSearch(String query, Pageable pageable);

	public List<Playlist> getNewestPlaylists();
	
	public List<Playlist> getTopRatedPlaylists();

	public Playlist createPlaylist(Long userID, String name, List<Video> videos, List<Mood> moods, List<Genre> genres) throws PlaylistExistsException;

	Page<HasPlaylist> getAdminPlaylists(Long userID, Pageable pageable);

	Page<HasPlaylist> getUsersPlaylists(Long userID, Pageable pageable);
}
