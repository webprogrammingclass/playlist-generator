package mk.ukim.finki.playlistgen.services.imp;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mk.ukim.finki.playlistgen.model.Genre;
import mk.ukim.finki.playlistgen.repository.GenreRepository;
import mk.ukim.finki.playlistgen.services.GenreService;

@Service
public class GenreServiceImp implements GenreService {
	@Autowired
	private GenreRepository genreRepository;
	
	@Override
	public boolean saveAllGenres(List<Genre> genres) {
		for(int i=0; i<genres.size(); i++) {
			Genre g = genreRepository.findByGenreNameIgnoreCase(genres.get(i).getGenreName());
			if(g == null)
				genreRepository.save(genres.get(i));
		}
		
		return true;
	}

	@Override
	public List<Genre> getAllGenres() {
		return genreRepository.findAll();
	}

	@Override
	public Genre getGenre(String id) {
		return genreRepository.findOne(Long.parseLong(id));
	}
}
